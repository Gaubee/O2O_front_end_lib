var modulesInit = jSouper.modulesInit;

modulesInit["main-goods-test"] = function(vm) {
	var node = vm.getOneElementByTagName("commodity");

	var eventManager = require("eventManager");

	function _filter_cash() {
		var finally_cash = vm.get("cash");
		var sou_cash = vm.get("price");
		var user_card_id_list = App.get("loginer.card_list") || [];
		jSouper.forEach(user_card_id_list, function(card_id) {
			var card_info = card_id.split(/[\:\|]/);
			var bus_id = card_info[1];
			if (bus_id == window.busInfo ? busInfo._id : appConfig.bus_id) {
				var card_factory_id = card_info[3];
				var new_finally_cash = (vm.get("card_cash_map") || {})[card_factory_id] || finally_cash;
				if (new_finally_cash !== finally_cash) {
					finally_cash = new_finally_cash;
				}
				return false;
			}
		});
		vm.set("$Private.finally_cash", finally_cash);
	}
	eventManager.is(App.get("loginer"), "getLoginer", _filter_cash);
	vm.model.onfollow = _filter_cash;
	jSouper.onElementPropertyChange(node, "goods-id", _filter_cash);
};
modulesInit["top_drop_down_navigation"] = function(vm) {
	var cabtn = vm.queryElement({
		name: "cancel"
	})[0];
	console.log(cabtn);
	require(["jQuery"], function($) {
		var cbtn = $(cabtn);
		cbtn.click(function() {
			a = $("dropdownnav")[0];
			$(a).animate({
				top: -265
			}, 800);
		});
	});
};
modulesInit["topnav"] = function(vm) {
	var getI = vm.queryElement({
		tagName: "I"
	})[2];
	console.log(getI);
	require(["jQuery"], function($) {
		var obj = $(getI);
		obj.click(function() {
			a = $("dropdownnav")[0];
			// alert(a.length);
			$(a).animate({
				top: 0
			}, 800);
		});
	});
};
modulesInit["commodity"] = function(vm) {
	var eachChildNode = vm.queryElement({
		tagName: "EACHCHILD"
	})[0];
	console.log(eachChildNode)
	require(["jQuery"], function($) {
		var imgs = $(eachChildNode);
		var img = imgs.find("img");
		var i = 1;
		timer = setInterval(function() {
			imgs.animate({
				bottom: 200 * (i % img.length)
			}, 800);
			i += 1;
		}, 2000);
	});
};
modulesInit["pic_wall"] = function(vm) {
	require(["jQuery"], function($) {
		var index = 0;
		var $_picn = $(".pic").length;
		setInterval(function() {
			show(index);
			index++;
			if (index == $_picn) {
				index = 0;
			}
		}, 5000);

		function show(index) {
			$(".pic").eq(index).fadeIn(800).siblings(".pic").fadeOut(800);
		}
	});
};
modulesInit["main-goods"] = function(vm) {
	var eachChild = vm.queryElement({
		className: "eachChild"
	})[0];
	var img = vm.queryElement({
			tagName: "IMG"
		})[0]
		// console.log($(eachChild));
	require(["jQuery"], function($) {
		var index = 0;
		var height = $(img).css("height").split("p")[0];
		timer = setInterval(function() {
			$(eachChild).animate({
				bottom: height * (index % 2)
			}, 800);
			index += 1;
		}, getRadom());
	});

	function getRadom() {
		var a = 0;
		while (a < 4000) {
			var a = Math.random() * 10000
		}
		console.log(a);
		return a;
	}
};
modulesInit["auto-clear-input"] = function(vm) {
	var input = vm.queryElement({
		tagName: "INPUT"
	})[0];
	require(["jQuery"], function($) {
		// var value=$(input).val();
		// $(input).focus(function(){
		//     if ($(input).val()==value) {
		//         $(input).val("");
		//         ;
		//     };
		// });
		//  $(input).blur(function(){
		//     if ($(input).val()=='') {
		//         $(input).val(value);
		//         ;
		//     };
		// });

		// $(input).keyup(function(){
		//     $(this).val($(this).val().replace(/[^\w\.\-\_\@/]/ig,''));}).bind("paste",function(){  //CTR+V事件处理 
		//         $(this).val($(this).val().replace(/[^\w\.\-\_\@/]/ig,''));  
		//     }).css("ime-mode", "disabled");  //CSS设置输入法不可用
	});
};
modulesInit["goods"] = function(vm) {
	var goods_box = vm.queryElement({
		className: "goods"
	})[0];
	var fg = vm.queryElement({
		tagName: "IMG"
	})[0];
	var bg = vm.queryElement({
		className: "details"
	})[0];
	require(["jQuery"], function($) {
		var name = $(goods_box).attr("name");
		// console.log(name);
		if (name % 4 == 0) {
			$(fg).mouseover(function() {
				$(goods_box).css("zIndex", "2");
				$(bg).stop().animate({
					right: 165,
					opacity: 100
				}, 300);
			});
			$(fg).mouseout(function() {
				$(bg).stop().animate({
					right: 0,
					opacity: 0
				}, 300);
				$(goods_box).css("zIndex", "1");
			});
		} else {
			$(fg).mouseover(function() {
				$(goods_box).css("zIndex", "2");
				$(bg).stop().animate({
					left: 165,
					opacity: 100
				}, 300);
			});
			$(fg).mouseout(function() {
				$(bg).stop().animate({
					left: 0,
					opacity: 0
				}, 300);
				$(goods_box).css("zIndex", "1");
			});
		}
	});
};
modulesInit["carts"] = function(vm) {
	var carts = vm.queryElement({
		className: "cart"
	})[0];
	var _cut_btn = vm.queryElement({
		className: "cut"
	})[0];
	var _add_btn = vm.queryElement({
		className: "plus"
	})[0];
	var input = vm.queryElement({
		tagName: "INPUT"
	})[0];
	var _remove_btn = vm.queryElement({
		className: "remove"
	})[0];
	require(["jQuery"], function($) {

		//增减
		$(_cut_btn).mousedown(function() {
			if ($(input).val() > 1) {
				$(input).val(parseInt($(input).val()) - 1);
			}
		});
		$(_add_btn).mousedown(function() {
			if ($(input).val() < 999) {
				$(input).val(parseInt($(input).val()) + 1);
			}
		});


		$(_remove_btn).click(function() {
			$(carts).remove();
		});

		$(input).change(function() {
			if ($(input).val() == "") {
				$(input).val(1);
			};
		})
		$(input).keyup(function() { //只能输入数字s
			$(this).val($(this).val().replace(/\D/g, ''));
		}).bind("paste", function() { //CTR+V事件处理 
			$(this).val($(this).val().replace(/\D/g, ''));
		}).css("ime-mode", "disabled"); //CSS设置输入法不可用

	})
};
modulesInit["goods-main-img"] = function(vm) {
	var imgWall;
	var imgBox;
	var smallImgs;
	vm.oninsert = function() {
		var ti = setInterval(function() {
			imgBox = $("#imgBox.role-img-box");
			if (imgBox.length) {
				smallImgs = $(".small-img");
				imgWall = $("#imgWall");
				clearInterval(ti);
				if (imgBox.height() > $(window).height()) {
					console.log(imgBox.find(".big-img").height());
					imgWall.css({
						height: (imgBox.find(".big-img").height() || imgBox.width()) + "px"
					});
				}
				_auto_animate();
			}
		});
	}
	var ii = 0;
	var ti;

	function _auto_animate(is_stop) {
		clearTimeout(ti);
		if (!is_stop) {
			ti = setTimeout(function() {
				smallImgs = $(".small-img");
				ii += 1;
				ii = ii % smallImgs.length;
				var _item = smallImgs[ii];
				if (_item) {
					change_img(_item);
				}
			}, 5000);
		}
	}

	function change_img(obj) {

		var i = ~~obj.getAttribute("_index");
		var $obj = $(obj);
		var button_length = 0;
		var bigImg;
		imgBox.find(".big-img").each(function(_i, img) {
			// console.log(_i,i);
			if (i > _i) {
				button_length += $(img).height();
			} else {
				bigImg = img;
				return false
			}
		});

		imgBox.stop().animate({
			"bottom": button_length + "px",
			'border': '1px solid red'
		}, 800);
		imgWall.css({
			"height": $(bigImg).height() + "px"
		});
		ii = i;
		smallImgs.css({
			// 'border':'0',
			opacity: 0.5
		});
		$obj.css({
			opacity: 1
		});
		_auto_animate();
	}
	window.change_img = change_img;
	//图
	var magnifierImgNode = vm.queryElement({
		className: "magnifier-img"
	})[0];
	//图容器
	var imgMagnifierNode = vm.queryElement({
		className: "img-magnifier"
	})[0];
	//坐标器
	var imgMagnifierMask = vm.queryElement({
		className: "img-magnifier-mask"
	})[0];
	vm.set("$Private.$Event.open_magnifier", function(e, cvm) {
		console.log("打开放大镜");
		vm.set("$Pirvate.$Cache.show_magnifier", true);
		vm.set("$Pirvate.$Cache.magnifier_src", cvm.get());
		_auto_animate(true);
	});
	vm.set("$Private.$Event.close_magnifier", function() {
		console.log("关闭放大镜");
		vm.set("$Pirvate.$Cache.show_magnifier", false);
		_auto_animate();
	});
	vm.set("$Private.$Event.move_magnifier", function(e) {
		var width = this.clientWidth;
		var height = this.clientHeight;
		var x = e.offsetX;
		var y = e.offsetY;
		// console.log("移动放大镜！");

		var wrap_width = imgMagnifierNode.clientWidth;
		var wrap_height = imgMagnifierNode.clientHeight;
		var mag_width = magnifierImgNode.clientWidth;
		var mag_height = magnifierImgNode.clientHeight;

		var __x = width / mag_width;
		var __y = height / mag_height;

		imgMagnifierMask.style.width = width * __x + "px";
		imgMagnifierMask.style.height = height * __y + "px";

		var __center_mouse_x = wrap_width / 2 * __x;
		var __center_mouse_y = wrap_height / 2 * __y;
		// console.log(__center_mouse_x, __center_mouse_y);
		if (wrap_width < mag_width) {
			var _top = 0;
			var _left = 0;
			if (x < __center_mouse_x) {
				x = __center_mouse_x;
			} else if (x > (width - __center_mouse_x)) {
				x = width - __center_mouse_x;
			}
			var _x = x - __center_mouse_x;
			_left = _x / __x;
			magnifierImgNode.style.left = "-" + _left + "px";
		} else { //水平居中
			magnifierImgNode.style.left = (wrap_width - mag_width) / 2 + "px"
		}
		if (wrap_height < mag_height) {
			if (y < __center_mouse_y) {
				y = __center_mouse_y;
			}
			if (y > (height - __center_mouse_y)) {
				y = height - __center_mouse_y;
			}
			var _y = y - __center_mouse_y;
			_top = _y / __y;
			magnifierImgNode.style.top = "-" + _top + "px";
		} else { //垂直居中
			magnifierImgNode.style.top = (wrap_height - mag_height) / 2 + "px"
		}
	});
};
modulesInit["m-goods-main-img"] = function(vm) {
	var imgWall;
	var imgBox;
	var smallImgs;
	vm.oninsert = function() {
		var ti = setInterval(function() {
			imgBox = $("#imgBox.role-img-box");
			if (imgBox.length) {
				smallImgs = $(".small-img");
				imgWall = $("#imgWall");
				clearInterval(ti);
				if (imgBox.width() > $(window).width()) {
					// console.log(imgBox.find(".big-img").height());
					imgWall.css({
						height: (imgBox.find(".big-img").height() || imgBox.width()) + "px"
					});
				}
				_auto_animate();
			}
		});
	}
	var ii = 0;
	var ti;

	function _auto_animate(argument) {
		clearTimeout(ti);
		ti = setTimeout(function() {
			// console.log("???xx",ii);
			ii += 1;
			ii = ii % smallImgs.length;
			var _item = smallImgs[ii];
			if (_item) {
				change_img(_item);
			}
		}, 5000);
	}

	function change_img(obj) {

		var i = ~~obj.getAttribute("_index");
		var $obj = $(obj);
		var button_length = 0;
		var bigImg;
		imgBox.find(".big-img").each(function(_i, img) {
			// console.log(_i,i);
			if (i > _i) {
				button_length += $(img).height();
			} else {
				bigImg = img;
				return false
			}
		});
		// console.log($(bigImg).height());
		imgBox.stop().animate({
			"right": button_length + "px",
			'border': '1px solid red'
		}, 800);
		imgWall.css({
			"height": $(bigImg).width() + "px"
		});
		ii = i;
		smallImgs.css({
			// 'border':'0',
			opacity: 0.5
		});
		$obj.css({
			opacity: 1
		});
		_auto_animate();
	}
	window.change_img = change_img;
};
modulesInit["goods-count"] = function(vm) {
	var _cut_btn = vm.queryElement({
		name: "cut"
	})[0];
	var _num = vm.queryElement({
		name: "num"
	})[0];
	var _add_btn = vm.queryElement({
		name: "add"
	})[0];
	// console.log(_cut_btn+"wow");
	require(["jQuery"], function($) {
		$(_cut_btn).click(function() {
			if ($(_num).val() > 1) {
				$(_num).val(parseInt($(_num).val()) - 1);
			}
		});

		$(_add_btn).click(function() {
			$(_num).val(parseInt($(_num).val()) + 1);
		});
	})
};
modulesInit["qrcode"] = function(vm) {
	var code_box = vm.queryElement({
		tagName: "DIV"
	})[0];
	var code_text = vm.queryElement({
		tagName: "INPUT"
	})[0];
	require(["jQuery", "jQuery.qrcode"], function($) {
		$(code_box).qrcode({
			render: window.dispatchEvent ? "canvas" : "table",
			width: 100,
			height: 100,
			text: $(code_text).val() //任意内容,Unicode编码,不支持中文
		});

	});
};
// modulesInit["user_ach_controller"] = function(vm) {

// };

function _init_express() {
	if (_init_express._is_init) {
		return
	}
	_init_express._is_init = true;
	var show_express_names = {
		"圆通速递": "yuantong",
		"申通快递": "shentong",
		"中通快递": "zhongtong",
		"韵达快递": "yunda",
		"EMS": "ems",
		"顺丰速运": "shunfeng",
		"百世汇通": "huitongkuaidi",
		"天天快递": "tiantian"
	};
	var express_names = {
		// "申通快递": "shentong",
		// "EMS": "ems",
		// "顺丰速运": "shunfeng",
		// "韵达快递": "yunda",
		// "圆通速递": "yuantong",
		// "中通快递": "zhongtong",
		// "百世汇通": "huitongkuaidi",
		// "天天快递": "tiantian",
		"宅急送": "zhaijisong",
		"鑫飞鸿": "xinhongyukuaidi",
		"CCES/国通快递": "cces",
		"全一快递": "quanyikuaidi",
		"彪记快递": "biaojikuaidi",
		"星晨急便": "xingchengjibian",
		"亚风速递": "yafengsudi",
		"源伟丰": "yuanweifeng",
		"全日通": "quanritongkuaidi",
		"安信达": "anxindakuaixi",
		"民航快递": "minghangkuaidi",
		"凤凰快递": "fenghuangkuaidi",
		"京广速递": "jinguangsudikuaijian",
		"配思货运": "peisihuoyunkuaidi",
		"中铁物流": "ztky",
		"UPS": "ups",
		"FedEx-国际件": "fedex",
		"TNT": "tnt",
		"DHL-中国件": "dhl",
		"AAE-中国件": "aae",
		"大田物流": "datianwuliu",
		"德邦物流": "debangwuliu",
		"新邦物流": "xinbangwuliu",
		"龙邦速递": "longbanwuliu",
		"一邦速递": "yibangwuliu",
		"速尔快递": "suer",
		"联昊通": "lianhaowuliu",
		"广东邮政": "guangdongyouzhengwuliu",
		"中邮物流": "zhongyouwuliu",
		"天地华宇": "tiandihuayu",
		"盛辉物流": "shenghuiwuliu",
		"长宇物流": "changyuwuliu",
		"飞康达": "feikangda",
		"元智捷诚": "yuanzhijiecheng",
		"邮政包裹/平邮": "youzhengguonei",
		"国际包裹": "youzhengguoji",
		"万家物流": "wanjiawuliu",
		"远成物流": "yuanchengwuliu",
		"信丰物流": "xinfengwuliu",
		"文捷航空": "wenjiesudi",
		"全晨快递": "quanchenkuaidi",
		"佳怡物流": "jiayiwuliu",
		"优速物流": "youshuwuliu",
		"快捷速递": "kuaijiesudi",
		"D速快递": "dsukuaidi",
		"全际通": "quanjitong",
		"能达速递": "ganzhongnengda",
		"青岛安捷快递": "anjiekuaidi",
		"越丰物流": "yuefengwuliu",
		"DPEX": "dpex",
		"急先达": "jixianda",
		"百福东方": "baifudongfang",
		"BHT": "bht",
		"伍圆速递": "wuyuansudi",
		"蓝镖快递": "lanbiaokuaidi",
		"COE": "coe",
		"南京100": "nanjing",
		"恒路物流": "hengluwuliu",
		"金大物流": "jindawuliu",
		"华夏龙": "huaxialongwuliu",
		"运通中港": "yuntongkuaidi",
		"佳吉快运": "jiajiwuliu",
		"盛丰物流": "shengfengwuliu",
		"源安达": "yuananda",
		"加运美": "jiayunmeiwuliu",
		"万象物流": "wanxiangwuliu",
		"宏品物流": "hongpinwuliu",
		"GLS": "gls",
		"上大物流": "shangda",
		"中铁快运": "zhongtiewuliu",
		"原飞航": "yuanfeihangwuliu",
		"海外环球": "haiwaihuanqiu",
		"三态速递": "santaisudi",
		"晋越快递": "jinyuekuaidi",
		"联邦快递": "lianbangkuaidi",
		"飞快达": "feikuaida",
		"全峰快递": "quanfengkuaidi",
		"如风达": "rufengda",
		"乐捷递": "lejiedi",
		"忠信达": "zhongxinda",
		"芝麻开门": "zhimakaimen",
		"赛澳递": "saiaodi",
		"海红网送": "haihongwangsong",
		"共速达": "gongsuda",
		"嘉里大通": "jialidatong",
		"OCS": "ocs",
		"USPS": "usps",
		"美国快递": "meiguokuaidi",
		"立即送": "lijisong",
		"银捷速递": "yinjiesudi",
		"门对门": "menduimen",
		"递四方": "disifang",
		"郑州建华": "zhengzhoujianhua",
		"河北建华": "hebeijianhua",
		"微特派": "weitepai",
		"DHL-德国件": "dhlde",
		"通和天下": "tonghetianxia",
		"EMS-国际件": "emsguoji",
		"FedEx-美国件": "fedexus",
		"风行天下": "fengxingtianxia",
		"康力物流": "kangliwuliu",
		"跨越速递": "kuayue",
		"海盟速递": "haimengsudi",
		"圣安物流": "shenganwuliu",
		"一统飞鸿": "yitongfeihong",
		"中速快递": "zhongsukuaidi",
		"新蛋奥硕": "neweggozzo",
		"OnTrac": "ontrac",
		"七天连锁": "sevendays",
		"明亮物流": "mingliangwuliu",
		"凡客配送": "vancl",
		"华企快运": "huaqikuaiyun",
		"城市100": "city100",
		"红马甲物流": "sxhongmajia",
		"穗佳物流": "suijiawuliu",
		"飞豹快递": "feibaokuaidi",
		"传喜物流": "chuanxiwuliu",
		"捷特快递": "jietekuaidi",
		"隆浪快递": "longlangkuaidi",
		"EMS-英文": "emsen",
		"中天万运": "zhongtianwanyun",
		"香港(HongKong Post)": "hkpost",
		"邦送物流": "bangsongwuliu",
		"国通快递": "guotongkuaidi",
		"澳大利亚邮政": "auspost",
		"加拿大邮政": "canpost",
		"加拿大邮政": "canpostfr",
		"UPS-全球件": "upsen",
		"TNT-全球件": "tnten",
		"DHL-全球件": "dhlen",
		"顺丰-美国件": "shunfengen",
		"汇强快递": "huiqiangkuaidi",
		"希优特": "xiyoutekuaidi",
		"昊盛物流": "haoshengwuliu",
		"尚橙物流": "shangcheng",
		"亿领速运": "yilingsuyun",
		"大洋物流": "dayangwuliu",
		"递达速运": "didasuyun",
		"易通达": "yitongda",
		"邮必佳": "youbijia",
		"亿顺航": "yishunhang",
		"飞狐快递": "feihukuaidi",
		"潇湘晨报": "xiaoxiangchenbao",
		"巴伦支": "balunzhi",
		"Aramex": "aramex",
		"闽盛快递": "minshengkuaidi",
		"佳惠尔": "syjiahuier",
		"民邦速递": "minbangsudi",
		"上海快通": "shanghaikuaitong",
		"北青小红帽": "xiaohongmao",
		"GSM": "gsm",
		"安能物流": "annengwuliu",
		"KCS": "kcs",
		"City-Link": "citylink",
		"店通快递": "diantongkuaidi",
		"凡宇快递": "fanyukuaidi",
		"平安达腾飞": "pingandatengfei",
		"广东通路": "guangdongtonglu",
		"中睿速递": "zhongruisudi",
		"快达物流": "kuaidawuliu",
		"佳吉快递": "jiajikuaidi",
		"ADP国际快递": "adp",
		"颿达国际快递": "fardarww",
		"颿达国际快递(英文)": "fandaguoji",
		"林道国际快递": "shlindao",
		"中外运速递(中文)": "sinoex",
		"中外运速递": "zhongwaiyun",
		"深圳德创物流": "dechuangwuliu",
		"林道国际快递(英文)": "ldxpres",
		"瑞典": "ruidianyouzheng",
		"Posten AB": "postenab",
		"偌亚奥国际快递": "nuoyaao",
		"城际速递": "chengjisudi",
		"祥龙运通物流": "xianglongyuntong",
		"品速心达快递": "pinsuxinda",
		"宇鑫物流": "yuxinwuliu",
		"陪行物流": "peixingwuliu",
		"户通物流": "hutongwuliu",
		"西安城联速递": "xianchengliansudi",
		"煜嘉物流": "yujiawuliu",
		"一柒国际物流": "yiqiguojiwuliu",
		"Fedex": "fedexcn",
		"联邦快递-英文": "lianbangkuaidien",
		"中通": "zhongtongphone",
		"赛澳递for买卖宝": "saiaodimmb",
		"上海无疆for买卖宝": "shanghaiwujiangmmb",
		"新加坡小包(Singapore Post)": "singpost",
		"音素快运": "yinsu",
		"南方传媒物流": "ndwl",
		"速呈宅配": "sucheng",
		"创一快递": "chuangyi",
		"云南滇驿物流": "dianyi",
		"重庆星程快递": "cqxingcheng",
		"四川星程快递": "scxingcheng",
		"贵州星程快递": "gzxingcheng",
		"运通中港快递": "ytkd",
		"gati官网英文通道": "gatien",
		"gati官网中文通道": "gaticn",
		"jcex官网通道": "jcex",
		"派尔快递官网通道": "peex",
		"凯信达官网通道": "kxda",
		"安达信官网通道": "advancing",
		"汇文官网通道": "huiwen",
		"亿翔官网通道": "yxexpress",
		"东红合作通道": "donghong",
		"飞远配送": "feiyuanvipshop",
		"好运来": "hlyex",
		"dpex国际": "dpexen",
		"增益速递": "zengyisudi",
		"四川快优达速递": "kuaiyouda",
		"日昱物流": "riyuwuliu",
		"速通物流": "sutongwuliu",
		"南京晟邦物流": "nanjingshengbang",
		"爱尔兰邮政(An Post)": "anposten",
		"日本": "japanposten",
		"丹麦邮政": "postdanmarken",
		"巴西邮政Brazil Post": "brazilposten",
		"荷兰邮政": "postnlcn",
		"PostNL": "postnl",
		"乌克兰EMS(EMS Ukraine)": "emsukrainecn",
		"EMS Ukraine": "emsukraine",
		"乌克兰邮政包裹": "ukrpostcn",
		"乌克兰小包、大包(UkrPost)": "ukrpost",
		"海红for买卖宝": "haihongmmb",
		"FedEx UK": "fedexuk",
		"FedEx-英国件": "fedexukcn",
		"叮咚快递": "dingdong",
		"DPD": "dpd",
		"UPS Freight": "upsfreight",
		"ABF": "abf",
		"Purolator": "purolator",
		"比利时": "bpost",
		"bpostint": "bpostinter",
		"LaserShip": "lasership",
		"英国大包、EMS": "parcelforce",
		"英国邮政大包EMS": "parcelforcecn",
		"YODEL": "yodel",
		"DHL Netherlands": "dhlnetherlands",
		"MyHermes": "myhermes",
		"DPD Germany": "dpdgermany",
		"Fastway Ireland": "fastway",
		"法国": "chronopostfra",
		"Selektvracht": "selektvracht",
		"蓝弧快递": "lanhukuaidi",
		"Belgium Post": "belgiumpost",
		"UPS Mail Innovations": "upsmailinno",
		"挪威": "postennorge",
		"瑞士邮政": "swisspostcn",
		"瑞士(Swiss Post)": "swisspost",
		"英国邮政小包": "royalmailcn",
		"英国小包": "royalmail",
		"DHL Benelux": "dhlbenelux",
		"Nova Poshta": "novaposhta",
		"DHL Poland": "dhlpoland",
		"Estes": "estes",
		"TNT UK": "tntuk",
		"Deltec Courier": "deltec",
		"OPEK": "opek",
		"DPD Poland": "dpdpoland",
		"Italy SDA": "italysad",
		"MRW": "mrw",
		"Chronopost Portugal": "chronopostport",
		"Correos de Espa?a": "correosdees",
		"Direct Link": "directlink",
		"ELTA Hellenic Post": "eltahell",
		"捷克": "ceskaposta",
		"Siodemka": "siodemka",
		"International Seur": "seur",
		"久易快递": "jiuyicn",
		"克罗地亚": "hrvatska",
		"Bulgarian Posts": "bulgarian",
		"Portugal Seur": "portugalseur",
		"EC-Firstclass": "ecfirstclass",
		"DTDC India": "dtdcindia",
		"Safexpress": "safexpress",
		"韩国": "koreapost",
		"TNT Australia": "tntau",
		"Thailand Thai Post": "thailand",
		"SkyNet Malaysia": "skynetmalaysia",
		"马来西亚小包": "malaysiapost",
		"马来西亚大包、EMS": "malaysiaems",
		"京东": "jd",
		"Saudi Post": "saudipost",
		"南非": "southafrican",
		"OCA Argentina": "ocaargen",
		"尼日利亚(Nigerian Postal)": "nigerianpost",
		"Correos Chile": "chile",
		"Israel Post": "israelpost",
		"Toll Priority": "tollpriority",
		"Estafeta": "estafeta",
		"港快速递": "gdkd",
		"Correos de Mexico": "mexico",
		"罗马尼亚": "romanian",
		"TNT Italy": "tntitaly",
		"Mexico Multipack": "multipack",
		"葡萄牙": "portugalctt",
		"Interlink Express": "interlink",
		"DPD UK": "dpduk",
		"华航快递": "hzpl",
		"Gati-KWE": "gatikwe",
		"Red Express": "redexpress",
		"Mexico Senda Express": "mexicodenda",
		"TCI XPS": "tcixps",
		"高铁速递": "hre",
		"新加坡EMS、大包(Singapore Speedpost)": "speedpost",
		"EMS国际": "emsinten",
		"Asendia USA": "asendiausa",
		"Chronopost France": "chronopostfren",
		"Poste Italiane Paccocelere": "italiane",
		"冠达快递": "gda",
		"出口易": "chukou1",
		"黄马甲": "huangmajia",
		"新干线快递": "anlexpress",
		"飞洋快递": "shipgce",
		"贝海国际速递": "xlobo",
		"阿联酋邮政": "emirates",
		"新顺丰": "nsf",
		"巴基斯坦邮政": "pakistan",
		"世运快递": "shiyunkuaidi",
		"合众速递(UCS）": "ucs",
		"阿富汗邮政": "afghan",
		"白俄罗斯": "belpost",
		"全通快运": "quantwl",
		"宅急便": "zhaijibian",
		"EFS Post": "efs",
		"TNT Post": "tntpostcn",
		"英脉物流": "gml",
		"广通速递": "gtongsudi",
		"东瀚物流": "donghanwl",
		"rpx": "rpx",
		"日日顺物流": "rrs",
		"黑猫雅玛多": "yamato",
		"华通快运": "htongexpress",
		"吉尔吉斯斯坦(Kyrgyz Post)": "kyrgyzpost",
		"拉脱维亚(Latvijas Pasts)": "latvia",
		"黎巴嫩(Liban Post)": "libanpost",
		"立陶宛": "lithuania",
		"马尔代夫(Maldives Post)": "maldives",
		"马耳他": "malta",
		"马其顿(Macedonian Post)": "macedonia",
		"新西兰": "newzealand",
		"摩尔多瓦": "moldova",
		"孟加拉国(EMS)": "bangladesh",
		"塞尔维亚(PE Post of Serbia)": "serbia",
		"塞浦路斯": "cypruspost",
		"突尼斯(EMS)": "tunisia",
		"乌兹别克斯坦": "uzbekistan",
		"新喀里多尼亚[法国]": "caledonia",
		"叙利亚": "republic",
		"亚美尼亚": "haypost",
		"也门": "yemen",
		"印度(India Post)": "india",
		"英国(大包,EMS)": "england",
		"约旦": "jordan",
		"越南小包(Vietnam Posts)": "vietnam",
		"黑山(Pošta Crne Gore)": "montenegro",
		"哥斯达黎加": "correos",
		"西安喜来快递": "xilaikd",
		"韩润物流": "hanrun",
		"格陵兰[丹麦]": "greenland",
		"菲律宾": "phlpost",
		"厄瓜多尔(Correos del Ecuador)": "ecuador",
		"冰岛(Iceland Post)": "iceland",
		"波兰小包(Poczta Polska)": "emonitoring",
		"阿尔巴尼亚(Posta shqipatre)": "albania",
		"阿鲁巴[荷兰]": "aruba",
		"埃及": "egypt",
		"爱尔兰(An Post)": "ireland",
		"爱沙尼亚(Eesti Post)": "omniva",
		"云豹国际货运": "leopard",
		"中外运空运": "sinoairinex",
		"上海昊宏国际货物": "hyk",
		"城晓国际快递": "ckeex",
		"匈牙利": "hungary",
		"澳门(Macau Post)": "macao",
		"台湾": "postserv",
		"北京EMS": "bjemstckj",
		"阿根廷(Correo Argentino)": "correoargentino",
		"快淘快递": "kuaitao",
		"秘鲁(SERPOST)": "peru",
		"印度尼西亚EMS(Pos Indonesia-EMS)": "indonesia",
		"哈萨克斯坦(Kazpost)": "kazpost",
		"立白宝凯物流": "lbbk",
		"百千诚物流": "bqcwl",
		"皇家物流": "pfcexpress",
		"法国小包(Colissimo)": "csuivi",
		"奥地利(Austrian Post)": "austria",
		"乌克兰小包、大包(UkrPoshta)": "ukraine",
		"乌干达(Posta Uganda)": "uganda",
		"阿塞拜疆EMS(EMS AzerExpressPost)": "azerbaijan",
		"芬兰(Itella Posti Oy)": "finland",
		"斯洛伐克(Slovenská Posta)": "slovak",
		"埃塞俄比亚(Ethiopian postal)": "ethiopia",
		"卢森堡(Luxembourg Post)": "luxembourg",
		"毛里求斯(Mauritius Post)": "mauritius",
		"文莱(Brunei Postal)": "brunei",
		"Quantium": "quantium",
		"坦桑尼亚": "tanzania",
		"阿曼": "oman",
		"直布罗陀[英国]": "gibraltar",
		"博源恒通": "byht",
		"越南EMS": "vnpost",
		"安迅物流": "anxl",
		"达方物流": "dfpost",
		"伙伴物流": "huoban",
		"天纵物流": "tianzong",
		"波黑": "bohei",
		"玻利维亚": "bolivia",
		"柬埔寨": "cambodia",
		"巴林": "bahrain",
		"纳米比亚": "namibia",
		"卢旺达": "rwanda",
		"莱索托": "lesotho",
		"肯尼亚": "kenya",
		"喀麦隆": "cameroon",
		"伯利兹": "belize",
		"巴拉圭": "paraguay",
		"十方通物流": "sfift",
		"飞鹰物流": "hnfy",
		"i-pacle": "iparcel",
		"鑫锐达": "bjxsrd",
		"百世物流": "baishiwuliu"
	};;
	(function() {
		var show_express_arr = [];
		jSouper.forEach(show_express_names, function(value, key) {
			show_express_arr.push({
				name: key,
				key: value
			});
		});
		jSouper.appReady(function() {
			App.set("$Cache.express", show_express_arr);
		});
		var express_arr = [];
		jSouper.forEach(express_names, function(value, key) {
			express_arr.push({
				name: key,
				value: key,
				search_data: value + key
			});
		});
		jSouper.appReady(function() {
			App.set("$Cache.all_express", express_arr);
		});
	}());
};
_init_express._is_init = false
modulesInit["bus_ach_controller.bus_exporess"] = function(vm) {
	vm.set("$Private.$Event.pays.send_goods", function() {
		var state = vm.get("info.state");
		if (state == appConfig.ACH_STATE["ol未发货"]) {
			var url = appConfig.bus["achs_ol未发货_to_ol未收货"];
		} else if (state == appConfig.ACH_STATE["未发货"]) {
			url = appConfig.bus["achs_未发货_to_已发货"];
		} else {
			alert("error", "错误的订单状态")
			return;
		}
		var express_info = vm.get("info.express_info");
		(window.coAjax || require("coAjax")).put(url + vm.get("_id"), {
			express_info: express_info
		}, function(result) {
			alert("物流设置成功，订单已设置为发货状态");
			vm.set("info.state", result.result);
		});
	});
	vm.set("$Private.$Event.pays.check_express", function() {
		if (vm.get("info.express_info.company")) {
			if (!vm.get("info.express_info.num")) {
				alert("error", "请输入正确的物流编号");
			};
		} else {
			alert("error", "请选择正确的物流公司");
		};
	});
	_init_express();
};
modulesInit["ach_controller.express"] = function(vm) {
	vm.set("$Private.$Event.pays.view_express", function() {
		vm.model.toggle('isexpress');
		(window.coAjax || require("coAjax")).get(appConfig.server_url + "express_search", {
			num: vm.get("info.express_info.num"),
			company: vm.get("info.express_info.company")
		}, function(result) {
			console.log(result.result);
			result.result.data.length = 10;
			vm.set("$Private.$Cache.express_data", result.result);
		})
	});
	vm.set('$Private.$Event.close.express', function() {
		vm.model.toggle('isexpress');
	});
};
modulesInit["save-view-pwd"] = function(vm) {
	var node = vm.getOneElementByTagName("save-view-pwd");;
	jSouper.onElementPropertyChange(node, "value", function(value, key) {
		vm.set("$Private.$Cache.show_value", Array(value.length).join("*"));
	}, true);
	vm.set("$Private.$Event.show_value", function() {

	})
};
var modulesInit = jSouper.modulesInit;

modulesInit["main-goods-test"] = function(vm) {
	var node = vm.getOneElementByTagName("commodity");

	var eventManager = require("eventManager");

	function _filter_cash() {
		var finally_cash = vm.get("cash");
		var sou_cash = vm.get("price");
		var user_card_id_list = App.get("loginer.card_list") || [];
		jSouper.forEach(user_card_id_list, function(card_id) {
			var card_info = card_id.split(/[\:\|]/);
			var bus_id = card_info[1];
			if (bus_id == window.busInfo ? busInfo._id : appConfig.bus_id) {
				var card_factory_id = card_info[3];
				var new_finally_cash = (vm.get("card_cash_map") || {})[card_factory_id] || finally_cash;
				if (new_finally_cash !== finally_cash) {
					finally_cash = new_finally_cash;
				}
				return false;
			}
		});
		vm.set("$Private.finally_cash", finally_cash);
	}
	eventManager.is(App.get("loginer"), "getLoginer", _filter_cash);
	vm.model.onfollow = _filter_cash;
	jSouper.onElementPropertyChange(node, "goods-id", _filter_cash);
};
modulesInit["top_drop_down_navigation"] = function(vm) {
	var cabtn = vm.queryElement({
		name: "cancel"
	})[0];
	console.log(cabtn);
	require(["jQuery"], function($) {
		var cbtn = $(cabtn);
		cbtn.click(function() {
			a = $("dropdownnav")[0];
			$(a).animate({
				top: -265
			}, 800);
		});
	});
};
modulesInit["topnav"] = function(vm) {
	var getI = vm.queryElement({
		tagName: "I"
	})[2];
	console.log(getI);
	require(["jQuery"], function($) {
		var obj = $(getI);
		obj.click(function() {
			a = $("dropdownnav")[0];
			// alert(a.length);
			$(a).animate({
				top: 0
			}, 800);
		});
	});
};
modulesInit["commodity"] = function(vm) {
	var eachChildNode = vm.queryElement({
		tagName: "EACHCHILD"
	})[0];
	console.log(eachChildNode)
	require(["jQuery"], function($) {
		var imgs = $(eachChildNode);
		var img = imgs.find("img");
		var i = 1;
		timer = setInterval(function() {
			imgs.animate({
				bottom: 200 * (i % img.length)
			}, 800);
			i += 1;
		}, 2000);
	});
};
modulesInit["pic_wall"] = function(vm) {
	require(["jQuery"], function($) {
		var index = 0;
		var $_picn = $(".pic").length;
		setInterval(function() {
			show(index);
			index++;
			if (index == $_picn) {
				index = 0;
			}
		}, 5000);

		function show(index) {
			$(".pic").eq(index).fadeIn(800).siblings(".pic").fadeOut(800);
		}
	});
};
modulesInit["main-goods"] = function(vm) {
	var eachChild = vm.queryElement({
		className: "eachChild"
	})[0];
	var img = vm.queryElement({
			tagName: "IMG"
		})[0]
		// console.log($(eachChild));
	require(["jQuery"], function($) {
		var index = 0;
		var height = $(img).css("height").split("p")[0];
		timer = setInterval(function() {
			$(eachChild).animate({
				bottom: height * (index % 2)
			}, 800);
			index += 1;
		}, getRadom());
	});

	function getRadom() {
		var a = 0;
		while (a < 4000) {
			var a = Math.random() * 10000
		}
		console.log(a);
		return a;
	}
};
modulesInit["auto-clear-input"] = function(vm) {
	var input = vm.queryElement({
		tagName: "INPUT"
	})[0];
	require(["jQuery"], function($) {
		// var value=$(input).val();
		// $(input).focus(function(){
		//     if ($(input).val()==value) {
		//         $(input).val("");
		//         ;
		//     };
		// });
		//  $(input).blur(function(){
		//     if ($(input).val()=='') {
		//         $(input).val(value);
		//         ;
		//     };
		// });

		// $(input).keyup(function(){
		//     $(this).val($(this).val().replace(/[^\w\.\-\_\@/]/ig,''));}).bind("paste",function(){  //CTR+V事件处理 
		//         $(this).val($(this).val().replace(/[^\w\.\-\_\@/]/ig,''));  
		//     }).css("ime-mode", "disabled");  //CSS设置输入法不可用
	});
};
modulesInit["goods"] = function(vm) {
	var goods_box = vm.queryElement({
		className: "goods"
	})[0];
	var fg = vm.queryElement({
		tagName: "IMG"
	})[0];
	var bg = vm.queryElement({
		className: "details"
	})[0];
	require(["jQuery"], function($) {
		var name = $(goods_box).attr("name");
		// console.log(name);
		if (name % 4 == 0) {
			$(fg).mouseover(function() {
				$(goods_box).css("zIndex", "2");
				$(bg).stop().animate({
					right: 165,
					opacity: 100
				}, 300);
			});
			$(fg).mouseout(function() {
				$(bg).stop().animate({
					right: 0,
					opacity: 0
				}, 300);
				$(goods_box).css("zIndex", "1");
			});
		} else {
			$(fg).mouseover(function() {
				$(goods_box).css("zIndex", "2");
				$(bg).stop().animate({
					left: 165,
					opacity: 100
				}, 300);
			});
			$(fg).mouseout(function() {
				$(bg).stop().animate({
					left: 0,
					opacity: 0
				}, 300);
				$(goods_box).css("zIndex", "1");
			});
		}
	});
};
modulesInit["carts"] = function(vm) {
	var carts = vm.queryElement({
		className: "cart"
	})[0];
	var _cut_btn = vm.queryElement({
		className: "cut"
	})[0];
	var _add_btn = vm.queryElement({
		className: "plus"
	})[0];
	var input = vm.queryElement({
		tagName: "INPUT"
	})[0];
	var _remove_btn = vm.queryElement({
		className: "remove"
	})[0];
	require(["jQuery"], function($) {

		//增减
		$(_cut_btn).mousedown(function() {
			if ($(input).val() > 1) {
				$(input).val(parseInt($(input).val()) - 1);
			}
		});
		$(_add_btn).mousedown(function() {
			if ($(input).val() < 999) {
				$(input).val(parseInt($(input).val()) + 1);
			}
		});


		$(_remove_btn).click(function() {
			$(carts).remove();
		});

		$(input).change(function() {
			if ($(input).val() == "") {
				$(input).val(1);
			};
		})
		$(input).keyup(function() { //只能输入数字s
			$(this).val($(this).val().replace(/\D/g, ''));
		}).bind("paste", function() { //CTR+V事件处理 
			$(this).val($(this).val().replace(/\D/g, ''));
		}).css("ime-mode", "disabled"); //CSS设置输入法不可用

	})
};
modulesInit["goods-main-img"] = function(vm) {
	var imgWall;
	var imgBox;
	var smallImgs;
	vm.oninsert = function() {
		var ti = setInterval(function() {
			imgBox = $("#imgBox.role-img-box");
			if (imgBox.length) {
				smallImgs = $(".small-img");
				imgWall = $("#imgWall");
				clearInterval(ti);
				if (imgBox.height() > $(window).height()) {
					console.log(imgBox.find(".big-img").height());
					imgWall.css({
						height: (imgBox.find(".big-img").height() || imgBox.width()) + "px"
					});
				}
				_auto_animate();
			}
		});
	}
	var ii = 0;
	var ti;

	function _auto_animate(is_stop) {
		clearTimeout(ti);
		if (!is_stop) {
			ti = setTimeout(function() {
				smallImgs = $(".small-img");
				ii += 1;
				ii = ii % smallImgs.length;
				var _item = smallImgs[ii];
				if (_item) {
					change_img(_item);
				}
			}, 5000);
		}
	}

	function change_img(obj) {

		var i = ~~obj.getAttribute("_index");
		var $obj = $(obj);
		var button_length = 0;
		var bigImg;
		imgBox.find(".big-img").each(function(_i, img) {
			// console.log(_i,i);
			if (i > _i) {
				button_length += $(img).height();
			} else {
				bigImg = img;
				return false
			}
		});

		imgBox.stop().animate({
			"bottom": button_length + "px",
			'border': '1px solid red'
		}, 800);
		imgWall.css({
			"height": $(bigImg).height() + "px"
		});
		ii = i;
		smallImgs.css({
			// 'border':'0',
			opacity: 0.5
		});
		$obj.css({
			opacity: 1
		});
		_auto_animate();
	}
	window.change_img = change_img;
	//图
	var magnifierImgNode = vm.queryElement({
		className: "magnifier-img"
	})[0];
	//图容器
	var imgMagnifierNode = vm.queryElement({
		className: "img-magnifier"
	})[0];
	//坐标器
	var imgMagnifierMask = vm.queryElement({
		className: "img-magnifier-mask"
	})[0];
	vm.set("$Private.$Event.open_magnifier", function(e, cvm) {
		console.log("打开放大镜");
		vm.set("$Pirvate.$Cache.show_magnifier", true);
		vm.set("$Pirvate.$Cache.magnifier_src", cvm.get());
		_auto_animate(true);
	});
	vm.set("$Private.$Event.close_magnifier", function() {
		console.log("关闭放大镜");
		vm.set("$Pirvate.$Cache.show_magnifier", false);
		_auto_animate();
	});
	vm.set("$Private.$Event.move_magnifier", function(e) {
		var width = this.clientWidth;
		var height = this.clientHeight;
		var x = e.offsetX;
		var y = e.offsetY;
		// console.log("移动放大镜！");

		var wrap_width = imgMagnifierNode.clientWidth;
		var wrap_height = imgMagnifierNode.clientHeight;
		var mag_width = magnifierImgNode.clientWidth;
		var mag_height = magnifierImgNode.clientHeight;

		var __x = width / mag_width;
		var __y = height / mag_height;

		imgMagnifierMask.style.width = width * __x + "px";
		imgMagnifierMask.style.height = height * __y + "px";

		var __center_mouse_x = wrap_width / 2 * __x;
		var __center_mouse_y = wrap_height / 2 * __y;
		// console.log(__center_mouse_x, __center_mouse_y);
		if (wrap_width < mag_width) {
			var _top = 0;
			var _left = 0;
			if (x < __center_mouse_x) {
				x = __center_mouse_x;
			} else if (x > (width - __center_mouse_x)) {
				x = width - __center_mouse_x;
			}
			var _x = x - __center_mouse_x;
			_left = _x / __x;
			magnifierImgNode.style.left = "-" + _left + "px";
		} else { //水平居中
			magnifierImgNode.style.left = (wrap_width - mag_width) / 2 + "px"
		}
		if (wrap_height < mag_height) {
			if (y < __center_mouse_y) {
				y = __center_mouse_y;
			}
			if (y > (height - __center_mouse_y)) {
				y = height - __center_mouse_y;
			}
			var _y = y - __center_mouse_y;
			_top = _y / __y;
			magnifierImgNode.style.top = "-" + _top + "px";
		} else { //垂直居中
			magnifierImgNode.style.top = (wrap_height - mag_height) / 2 + "px"
		}
	});
};
modulesInit["m-goods-main-img"] = function(vm) {
	var imgWall;
	var imgBox;
	var smallImgs;
	vm.oninsert = function() {
		var ti = setInterval(function() {
			imgBox = $("#imgBox.role-img-box");
			if (imgBox.length) {
				smallImgs = $(".small-img");
				imgWall = $("#imgWall");
				clearInterval(ti);
				if (imgBox.width() > $(window).width()) {
					// console.log(imgBox.find(".big-img").height());
					imgWall.css({
						height: (imgBox.find(".big-img").height() || imgBox.width()) + "px"
					});
				}
				_auto_animate();
			}
		});
	}
	var ii = 0;
	var ti;

	function _auto_animate(argument) {
		clearTimeout(ti);
		ti = setTimeout(function() {
			// console.log("???xx",ii);
			ii += 1;
			ii = ii % smallImgs.length;
			var _item = smallImgs[ii];
			if (_item) {
				change_img(_item);
			}
		}, 5000);
	}

	function change_img(obj) {

		var i = ~~obj.getAttribute("_index");
		var $obj = $(obj);
		var button_length = 0;
		var bigImg;
		imgBox.find(".big-img").each(function(_i, img) {
			// console.log(_i,i);
			if (i > _i) {
				button_length += $(img).height();
			} else {
				bigImg = img;
				return false
			}
		});
		// console.log($(bigImg).height());
		imgBox.stop().animate({
			"right": button_length + "px",
			'border': '1px solid red'
		}, 800);
		imgWall.css({
			"height": $(bigImg).width() + "px"
		});
		ii = i;
		smallImgs.css({
			// 'border':'0',
			opacity: 0.5
		});
		$obj.css({
			opacity: 1
		});
		_auto_animate();
	}
	window.change_img = change_img;
};
modulesInit["goods-count"] = function(vm) {
	var _cut_btn = vm.queryElement({
		name: "cut"
	})[0];
	var _num = vm.queryElement({
		name: "num"
	})[0];
	var _add_btn = vm.queryElement({
		name: "add"
	})[0];
	// console.log(_cut_btn+"wow");
	require(["jQuery"], function($) {
		$(_cut_btn).click(function() {
			if ($(_num).val() > 1) {
				$(_num).val(parseInt($(_num).val()) - 1);
			}
		});

		$(_add_btn).click(function() {
			$(_num).val(parseInt($(_num).val()) + 1);
		});
	})
};
modulesInit["qrcode"] = function(vm) {
	var code_box = vm.queryElement({
		tagName: "DIV"
	})[0];
	var code_text = vm.queryElement({
		tagName: "INPUT"
	})[0];
	require(["jQuery", "jQuery.qrcode"], function($) {
		$(code_box).qrcode({
			render: window.dispatchEvent ? "canvas" : "table",
			width: 100,
			height: 100,
			text: $(code_text).val() //任意内容,Unicode编码,不支持中文
		});

	});
};
// modulesInit["user_ach_controller"] = function(vm) {

// };
function _user_ach_controller_cancel_ach(e, vm) {
	(window.coAjax || require("coAjax"))["delete"](appConfig.user.ach_cancel + vm.get("_id"), function(result) {
		alert("success", "订单取消成功");
		var on_cancel_ach_success = vm.get("$Event.cancel_ach_success") || vm.get("$Top.$Event.cancel_ach_success");
		on_cancel_ach_success && on_cancel_ach_success(vm, result);
	});
};
modulesInit["user_ach_controller.ACH_STATE.-1"] = function(vm) {
	var _auto_go_online_pay = vm.get("_auto_go_online_pay");
	//确定订单
	vm.set("$Private.$Event.pays.lock_ach", function(e) {
		var ach = vm.get();
		(window.coAjax || require("coAjax")).put(appConfig.user.ach_lock + ach._id, function(result) {
			var ach = result.result;
			ach._auto_go_online_pay = _auto_go_online_pay;
			vm.set(ach);
			// App.set("submitOrder", true);
			// var init_time = 15;
			// var _timer = setInterval(function() {
			// 	App.set("init_time", init_time);
			// 	init_time -= 1;
			// 	if (init_time < 0) {
			// 		App.set("submitOrder", false);
			// 		clearInterval(_timer);
			// 	};
			// }, 1000);
			// vm.get("$Private.$Event.pays.alipay_build")();
		}, function(errorCode, xhr, errorMsg) {
			alert("error", errorMsg);
			if (window.payWindow) {
				payWindow.close();
				window.payWindow = null;
			}
		});
		//如果是要自动跳到在线支付的订单，在这次点击的时候打开一个窗口
		if (_auto_go_online_pay) {
			var _open_pay_window = false;
			if (ach.info.pay_method === appConfig.PAY_METHOD.担保交易) { //PC支付宝需要
				_open_pay_window = true;
			}
			/*手机微信不能使用open，所以还是用jump
			else if (ach.info.pay_method === appConfig.PAY_METHOD.weixin_pay && window._isWX) { //手机微信的微信支付需要跳转到统一支付页
				_open_pay_window = true;
			}*/
			if (_open_pay_window) {
				window.payWindow = window.open();
				payWindow.document.write("<center><h2>跳转中，请稍等……</h2></center>");
			}
		}
	});
	//取消订单
	vm.set("$Private.$Event.pays.cancel_ach", _user_ach_controller_cancel_ach);

};
modulesInit["user_ach_controller.ACH_STATE.11"] = function(vm) {

	//支付宝
	vm.set("$Private.$Event.pays.alipay_build", function(e) {
		vm.set("$Private._is_pop", true);
		// (window.coAjax || require("coAjax")).post(appConfig.alipay.create_direct_pay_by_user, {
		// 	WIDout_trade_no: vm.get("_id"),
		// 	WIDsubject: "来自“" + vm.get("info.bus_id") + "”的订单",
		// 	// WIDtotal_fee: vm.get("finally_cash"),
		// 	WIDbody: "",
		// 	WIDshow_url: location.href
		// }, function(result) {
		// 	vm.set("$Private.$Cache.alipay_submit_pay_ach", result.toString);
		// });
		(window.coAjax || require("coAjax")).post(appConfig.alipay.create_direct_pay_by_user_form_ach_id + vm.get("_id"), {
			WIDsubject: "来自“" + vm.get("info.bus_id") + "”的订单",
			WIDshow_url: location.href,
			type: "url"
		}, function(result) {
			vm.set("$Private.$Cache.alipay_submit_pay_ach", result.toString);
		});

	});
	//支付宝网页
	vm.set("$Private.$Event.pays.alipay_web_build", function(e, vm) {
		vm.set("$Private._is_pop", true);
		if (vm.get("$Private.$Cache.alipay_submit_pay_ach")) {
			//已经请求过了支付按钮，不再重复请求
			return;
		}
		(window.coAjax || require("coAjax")).post(appConfig.alipay.wap_trade_create_direct_form_ach_id + vm.get("_id"), {
			WIDsubject: "来自“" + vm.get("info.bus_id") + "”的订单",
			WIDshow_url: location.href,
			type: "url"
		}, function(result) {
			vm.set("$Private.$Cache.alipay_submit_pay_ach", result.toString);
		});
	});
	//财付通
	vm.set("$Private.$Event.pays.tenpay_build", function() {
		vm.set("$Private._is_pop", true);
		(window.coAjax || require("coAjax")).post(appConfig.tenpay.create_direct_pay_by_user, {
			ach_id: vm.get("_id"),
			title: "来自“" + vm.get("info.bus_id") + "”的订单",
			remark: "",
			show_url: location.href
		}, function(result) {
			vm.set("$Private.$Cache.alipay_submit_pay_ach", result.toString);
		});
	});
	//微信支付
	//获取支付信息
	function _weixin_pay() {
		globalGet("WEIXIN_OPENID:" + (window.busInfo ? busInfo._id : appConfig.bus_id), function(openid) {
			(window.coAjax || require("coAjax")).post(appConfig.wx.js_unified_order, {
				ach_id: vm.get("_id"),
				detail: "",
				openid: openid
			}, function(result) {
				var wx_pay = result.result;
				// //得到支付配置，显示确定按钮
				vm.set("$Private.$Cache.wx_js_pay", wx_pay);
			});
		});
	};
	//获取支付者ID，显示支付按钮
	vm.set("$Private.$Event.pays.weixin_pay", function() {
		var buttonNode = this;
		if (_isWX) { //手机微信端，使用微信支付
			vm.set("$Private._is_pop", true);
			require(["WX"], function(WX) {
				alert("wx require ok");
				WX(_weixin_pay);
			});
		} else { //否则使用扫码字符
			vm.set("$Private.$Cache.wx_native_pay_loading", true);
			buttonNode.disabled = true;
			(window.coAjax || require("coAjax")).get(appConfig.wx.get_ach_url_code + vm.get("_id"), function(result) {
				var unifiedOrder = result.result;
				vm.set("$Private.$Cache.wx_native_pay", unifiedOrder);
				vm.set("$Private.$Cache.wx_native_pay_loading", false);
			}, function(errorCode, xhr, errorMsg) {
				alert("error", errorMsg)
				vm.set("$Private.$Cache.wx_native_pay_loading", false);
				buttonNode.disabled = false;
			});
		}
	});
	//微信支付按钮
	vm.set("$Private.$Event.pays.open_weixin_js_pay", function() {
		var wx_pay = vm.get("$Private.$Cache.wx_js_pay");
		var wx_pay_config = {
			timestamp: wx_pay.timeStamp,
			nonceStr: wx_pay.nonceStr,
			package: wx_pay.package,
			signType: wx_pay.signType,
			paySign: wx_pay.paySign
		};
		wx_pay_config = JSON.stringify(wx_pay_config);
		globalSet("wx_pay_config", wx_pay_config, function() {
			var url = "http://www.dotnar.com/proxy_wx_pay.html?wx_bus_id=" + encodeURIComponent(window.busInfo ? busInfo._id : appConfig.bus_id)
			if (window.Path) {
				Path.jump(url);
			} else {
				require("href").jump(url);
			}
		}, function() {
			var url = "http://www.dotnar.com/proxy_wx_pay.html?wx_bus_id=" + encodeURIComponent(window.busInfo ? busInfo._id : appConfig.bus_id) + "&wx_pay_config=" + encodeURIComponent(wx_pay_config)
			if (window.Path) {
				Path.jump(url);
			} else {
				require("href").jump(url);
			}
		});
		// wx_pay_config = encodeURIComponent(wx_pay_config);
	});
	//遮罩取消
	vm.set("$Private.$Event.pays.cancel", function(e, vm) {
		vm.set("$Private._is_pop", false);
	});
	//取消订单
	vm.set("$Private.$Event.pays.cancel_ach", _user_ach_controller_cancel_ach);

	//自动点击按钮完成流程
	if (vm.get("_auto_go_online_pay")) {
		//这个模块被注册出来的情况，都是订单信息已经有的情况下，所以不用考虑onfollow
		//点击 去支付
		var go_pay_button = vm.topNode().getElementsByClassName('method-item')[0];
		jSouper.dispatchEvent(go_pay_button, "click");
		//点击确定
		if (window.payWindow) { //如果之前点击了提交订单，那么就可以由payWindow对象
			vm.on("$Private.$Cache.alipay_submit_pay_ach", function(value, key, pm) {
				if (value) {
					//等待绑定的完全触发，自动点击pop的确定按钮
					setTimeout(function() {
						payWindow.location.href = value;
					}, 10)
				}
			});
		}
		vm.on("$Private.$Cache.wx_js_pay", function(value, key, pm) {
			if (value) {
				vm.get("$Private.$Event.pays.open_weixin_js_pay")();
			}
		});
		// vm.model.onfollow = function() {
		// 	debugger
		// }
	}
};
modulesInit["user_ach_controller.ACH_STATE.13"] = modulesInit["user_ach_controller.ACH_STATE.21"] = function(vm) {
	//确认收货
	vm.set("$Private.$Event.pays.confirm_goods", function() {
		var url;
		var state = vm.get("info.state");
		if (state == appConfig.ACH_STATE["ol未收货"]) {
			url = appConfig.user["ach_ol未收货_to_ol交易完成"]
		} else if (state == appConfig.ACH_STATE["已发货"]) {
			url = appConfig.user["ach_已发货_to_未收货未付款"]
		} else {
			alert("error", "错误的订单状态")
			return;
		}
		(window.coAjax || require("coAjax")).put(url + vm.get("_id"), function(result) {
			alert("成功确认收获");
			console.log(result.result);
			vm.set("info.state", result.result);
		});
	});
};
modulesInit["user_ach_controller.ACH_STATE.14"] = function(vm) {
	// 去评价
	vm.set("$Private.$Event.jumpToAppraise", function(e, vm) {
		require("href").jump("personal-beta.html#personal/appraise?oid=" + vm.get("_id"));
	})
};
modulesInit["user_ach_controller.ACH_STATE.20"] = modulesInit["user_ach_controller.ACH_STATE.30"] = function(vm) {
	//取消订单
	vm.set("$Private.$Event.pays.cancel_ach", _user_ach_controller_cancel_ach);
};
// modulesInit["bus_ach_controller"] = function(vm) {};
modulesInit["bus_ach_controller.ACH_STATE.11"] = function(vm) {
	//测试用的方法
	vm.set("$Private.$Event.pays.pay_under_line", function() {
		(window.coAjax || require("coAjax")).post(appConfig.server_url + "alipay/create_direct_pay_by_user/notify_url/succ", {
			WIDout_trade_no: vm.get("_id")
		}, function(result) {
			vm.set("info.state", +vm.get("info.state") + 1);
		})
	});
};
modulesInit["bus_ach_controller.ACH_STATE.22"] = function(vm) {
	vm.set("$Private.$Event.pays.未收货未付款_to_交易完成", function() {
		(window.coAjax || require("coAjax")).put(appConfig.bus["achs_未收货未付款_to_交易完成"] + vm.get("_id"), function(result) {
			alert("success", "交易完成");
			vm.set("info.state", result.result);
		});
	});
};
modulesInit["bus_ach_controller.ACH_STATE.30"] = function(vm) {
	vm.set("$Private.$Event.pays.face未付款_to_face交易完成", function() {
		(window.coAjax || require("coAjax")).put(appConfig.bus["achs_face未付款_to_face交易完成"] + vm.get("_id"), function(result) {
			alert("success", "交易完成");
			vm.set("info.state", result.result);
		});
	});
};
modulesInit["bus_ach_controller.custom_cash"] = function(vm) {
	function _is_updating(value) {
		vm.set("$Private.Cache.pays.cus_cash", 0);
		vm.set("$Private.$Cache.pays.is_updating_cash", !!value);
	};
	// var _cache_cash = vm.get("cus_cash")||vm.get("cash");
	vm.set("$Private.$Event.pays.update_cash", function() {
		_is_updating(true);
	});
	vm.set("$Private.$Event.pays.cancel_update_cash", function() {
		_is_updating(false);
	});
	vm.set("$Private.$Event.pays.upload_update_cash", function() {
		var cus_cash = parseFloat(vm.get("$Private.Cache.pays.cus_cash") || vm.get("cus_cash"));
		if (!cus_cash) {
			alert("error", "请输入正确的金额");
		}
		(window.coAjax || require("coAjax")).put(appConfig.bus.achs_custom_cash + vm.get("_id"), {
			cus_cash: cus_cash
		}, function(result) {
			alert("success", "修改成功");
			vm.set(result.result);
			_is_updating(false);
		});
	});
};

function _init_express() {
	if (_init_express._is_init) {
		return
	}
	_init_express._is_init = true;
	var show_express_names = {
		"圆通速递": "yuantong",
		"申通快递": "shentong",
		"中通快递": "zhongtong",
		"韵达快递": "yunda",
		"EMS": "ems",
		"顺丰速运": "shunfeng",
		"百世汇通": "huitongkuaidi",
		"天天快递": "tiantian"
	};
	var express_names = {
		// "申通快递": "shentong",
		// "EMS": "ems",
		// "顺丰速运": "shunfeng",
		// "韵达快递": "yunda",
		// "圆通速递": "yuantong",
		// "中通快递": "zhongtong",
		// "百世汇通": "huitongkuaidi",
		// "天天快递": "tiantian",
		"宅急送": "zhaijisong",
		"鑫飞鸿": "xinhongyukuaidi",
		"CCES/国通快递": "cces",
		"全一快递": "quanyikuaidi",
		"彪记快递": "biaojikuaidi",
		"星晨急便": "xingchengjibian",
		"亚风速递": "yafengsudi",
		"源伟丰": "yuanweifeng",
		"全日通": "quanritongkuaidi",
		"安信达": "anxindakuaixi",
		"民航快递": "minghangkuaidi",
		"凤凰快递": "fenghuangkuaidi",
		"京广速递": "jinguangsudikuaijian",
		"配思货运": "peisihuoyunkuaidi",
		"中铁物流": "ztky",
		"UPS": "ups",
		"FedEx-国际件": "fedex",
		"TNT": "tnt",
		"DHL-中国件": "dhl",
		"AAE-中国件": "aae",
		"大田物流": "datianwuliu",
		"德邦物流": "debangwuliu",
		"新邦物流": "xinbangwuliu",
		"龙邦速递": "longbanwuliu",
		"一邦速递": "yibangwuliu",
		"速尔快递": "suer",
		"联昊通": "lianhaowuliu",
		"广东邮政": "guangdongyouzhengwuliu",
		"中邮物流": "zhongyouwuliu",
		"天地华宇": "tiandihuayu",
		"盛辉物流": "shenghuiwuliu",
		"长宇物流": "changyuwuliu",
		"飞康达": "feikangda",
		"元智捷诚": "yuanzhijiecheng",
		"邮政包裹/平邮": "youzhengguonei",
		"国际包裹": "youzhengguoji",
		"万家物流": "wanjiawuliu",
		"远成物流": "yuanchengwuliu",
		"信丰物流": "xinfengwuliu",
		"文捷航空": "wenjiesudi",
		"全晨快递": "quanchenkuaidi",
		"佳怡物流": "jiayiwuliu",
		"优速物流": "youshuwuliu",
		"快捷速递": "kuaijiesudi",
		"D速快递": "dsukuaidi",
		"全际通": "quanjitong",
		"能达速递": "ganzhongnengda",
		"青岛安捷快递": "anjiekuaidi",
		"越丰物流": "yuefengwuliu",
		"DPEX": "dpex",
		"急先达": "jixianda",
		"百福东方": "baifudongfang",
		"BHT": "bht",
		"伍圆速递": "wuyuansudi",
		"蓝镖快递": "lanbiaokuaidi",
		"COE": "coe",
		"南京100": "nanjing",
		"恒路物流": "hengluwuliu",
		"金大物流": "jindawuliu",
		"华夏龙": "huaxialongwuliu",
		"运通中港": "yuntongkuaidi",
		"佳吉快运": "jiajiwuliu",
		"盛丰物流": "shengfengwuliu",
		"源安达": "yuananda",
		"加运美": "jiayunmeiwuliu",
		"万象物流": "wanxiangwuliu",
		"宏品物流": "hongpinwuliu",
		"GLS": "gls",
		"上大物流": "shangda",
		"中铁快运": "zhongtiewuliu",
		"原飞航": "yuanfeihangwuliu",
		"海外环球": "haiwaihuanqiu",
		"三态速递": "santaisudi",
		"晋越快递": "jinyuekuaidi",
		"联邦快递": "lianbangkuaidi",
		"飞快达": "feikuaida",
		"全峰快递": "quanfengkuaidi",
		"如风达": "rufengda",
		"乐捷递": "lejiedi",
		"忠信达": "zhongxinda",
		"芝麻开门": "zhimakaimen",
		"赛澳递": "saiaodi",
		"海红网送": "haihongwangsong",
		"共速达": "gongsuda",
		"嘉里大通": "jialidatong",
		"OCS": "ocs",
		"USPS": "usps",
		"美国快递": "meiguokuaidi",
		"立即送": "lijisong",
		"银捷速递": "yinjiesudi",
		"门对门": "menduimen",
		"递四方": "disifang",
		"郑州建华": "zhengzhoujianhua",
		"河北建华": "hebeijianhua",
		"微特派": "weitepai",
		"DHL-德国件": "dhlde",
		"通和天下": "tonghetianxia",
		"EMS-国际件": "emsguoji",
		"FedEx-美国件": "fedexus",
		"风行天下": "fengxingtianxia",
		"康力物流": "kangliwuliu",
		"跨越速递": "kuayue",
		"海盟速递": "haimengsudi",
		"圣安物流": "shenganwuliu",
		"一统飞鸿": "yitongfeihong",
		"中速快递": "zhongsukuaidi",
		"新蛋奥硕": "neweggozzo",
		"OnTrac": "ontrac",
		"七天连锁": "sevendays",
		"明亮物流": "mingliangwuliu",
		"凡客配送": "vancl",
		"华企快运": "huaqikuaiyun",
		"城市100": "city100",
		"红马甲物流": "sxhongmajia",
		"穗佳物流": "suijiawuliu",
		"飞豹快递": "feibaokuaidi",
		"传喜物流": "chuanxiwuliu",
		"捷特快递": "jietekuaidi",
		"隆浪快递": "longlangkuaidi",
		"EMS-英文": "emsen",
		"中天万运": "zhongtianwanyun",
		"香港(HongKong Post)": "hkpost",
		"邦送物流": "bangsongwuliu",
		"国通快递": "guotongkuaidi",
		"澳大利亚邮政": "auspost",
		"加拿大邮政": "canpost",
		"加拿大邮政": "canpostfr",
		"UPS-全球件": "upsen",
		"TNT-全球件": "tnten",
		"DHL-全球件": "dhlen",
		"顺丰-美国件": "shunfengen",
		"汇强快递": "huiqiangkuaidi",
		"希优特": "xiyoutekuaidi",
		"昊盛物流": "haoshengwuliu",
		"尚橙物流": "shangcheng",
		"亿领速运": "yilingsuyun",
		"大洋物流": "dayangwuliu",
		"递达速运": "didasuyun",
		"易通达": "yitongda",
		"邮必佳": "youbijia",
		"亿顺航": "yishunhang",
		"飞狐快递": "feihukuaidi",
		"潇湘晨报": "xiaoxiangchenbao",
		"巴伦支": "balunzhi",
		"Aramex": "aramex",
		"闽盛快递": "minshengkuaidi",
		"佳惠尔": "syjiahuier",
		"民邦速递": "minbangsudi",
		"上海快通": "shanghaikuaitong",
		"北青小红帽": "xiaohongmao",
		"GSM": "gsm",
		"安能物流": "annengwuliu",
		"KCS": "kcs",
		"City-Link": "citylink",
		"店通快递": "diantongkuaidi",
		"凡宇快递": "fanyukuaidi",
		"平安达腾飞": "pingandatengfei",
		"广东通路": "guangdongtonglu",
		"中睿速递": "zhongruisudi",
		"快达物流": "kuaidawuliu",
		"佳吉快递": "jiajikuaidi",
		"ADP国际快递": "adp",
		"颿达国际快递": "fardarww",
		"颿达国际快递(英文)": "fandaguoji",
		"林道国际快递": "shlindao",
		"中外运速递(中文)": "sinoex",
		"中外运速递": "zhongwaiyun",
		"深圳德创物流": "dechuangwuliu",
		"林道国际快递(英文)": "ldxpres",
		"瑞典": "ruidianyouzheng",
		"Posten AB": "postenab",
		"偌亚奥国际快递": "nuoyaao",
		"城际速递": "chengjisudi",
		"祥龙运通物流": "xianglongyuntong",
		"品速心达快递": "pinsuxinda",
		"宇鑫物流": "yuxinwuliu",
		"陪行物流": "peixingwuliu",
		"户通物流": "hutongwuliu",
		"西安城联速递": "xianchengliansudi",
		"煜嘉物流": "yujiawuliu",
		"一柒国际物流": "yiqiguojiwuliu",
		"Fedex": "fedexcn",
		"联邦快递-英文": "lianbangkuaidien",
		"中通": "zhongtongphone",
		"赛澳递for买卖宝": "saiaodimmb",
		"上海无疆for买卖宝": "shanghaiwujiangmmb",
		"新加坡小包(Singapore Post)": "singpost",
		"音素快运": "yinsu",
		"南方传媒物流": "ndwl",
		"速呈宅配": "sucheng",
		"创一快递": "chuangyi",
		"云南滇驿物流": "dianyi",
		"重庆星程快递": "cqxingcheng",
		"四川星程快递": "scxingcheng",
		"贵州星程快递": "gzxingcheng",
		"运通中港快递": "ytkd",
		"gati官网英文通道": "gatien",
		"gati官网中文通道": "gaticn",
		"jcex官网通道": "jcex",
		"派尔快递官网通道": "peex",
		"凯信达官网通道": "kxda",
		"安达信官网通道": "advancing",
		"汇文官网通道": "huiwen",
		"亿翔官网通道": "yxexpress",
		"东红合作通道": "donghong",
		"飞远配送": "feiyuanvipshop",
		"好运来": "hlyex",
		"dpex国际": "dpexen",
		"增益速递": "zengyisudi",
		"四川快优达速递": "kuaiyouda",
		"日昱物流": "riyuwuliu",
		"速通物流": "sutongwuliu",
		"南京晟邦物流": "nanjingshengbang",
		"爱尔兰邮政(An Post)": "anposten",
		"日本": "japanposten",
		"丹麦邮政": "postdanmarken",
		"巴西邮政Brazil Post": "brazilposten",
		"荷兰邮政": "postnlcn",
		"PostNL": "postnl",
		"乌克兰EMS(EMS Ukraine)": "emsukrainecn",
		"EMS Ukraine": "emsukraine",
		"乌克兰邮政包裹": "ukrpostcn",
		"乌克兰小包、大包(UkrPost)": "ukrpost",
		"海红for买卖宝": "haihongmmb",
		"FedEx UK": "fedexuk",
		"FedEx-英国件": "fedexukcn",
		"叮咚快递": "dingdong",
		"DPD": "dpd",
		"UPS Freight": "upsfreight",
		"ABF": "abf",
		"Purolator": "purolator",
		"比利时": "bpost",
		"bpostint": "bpostinter",
		"LaserShip": "lasership",
		"英国大包、EMS": "parcelforce",
		"英国邮政大包EMS": "parcelforcecn",
		"YODEL": "yodel",
		"DHL Netherlands": "dhlnetherlands",
		"MyHermes": "myhermes",
		"DPD Germany": "dpdgermany",
		"Fastway Ireland": "fastway",
		"法国": "chronopostfra",
		"Selektvracht": "selektvracht",
		"蓝弧快递": "lanhukuaidi",
		"Belgium Post": "belgiumpost",
		"UPS Mail Innovations": "upsmailinno",
		"挪威": "postennorge",
		"瑞士邮政": "swisspostcn",
		"瑞士(Swiss Post)": "swisspost",
		"英国邮政小包": "royalmailcn",
		"英国小包": "royalmail",
		"DHL Benelux": "dhlbenelux",
		"Nova Poshta": "novaposhta",
		"DHL Poland": "dhlpoland",
		"Estes": "estes",
		"TNT UK": "tntuk",
		"Deltec Courier": "deltec",
		"OPEK": "opek",
		"DPD Poland": "dpdpoland",
		"Italy SDA": "italysad",
		"MRW": "mrw",
		"Chronopost Portugal": "chronopostport",
		"Correos de Espa?a": "correosdees",
		"Direct Link": "directlink",
		"ELTA Hellenic Post": "eltahell",
		"捷克": "ceskaposta",
		"Siodemka": "siodemka",
		"International Seur": "seur",
		"久易快递": "jiuyicn",
		"克罗地亚": "hrvatska",
		"Bulgarian Posts": "bulgarian",
		"Portugal Seur": "portugalseur",
		"EC-Firstclass": "ecfirstclass",
		"DTDC India": "dtdcindia",
		"Safexpress": "safexpress",
		"韩国": "koreapost",
		"TNT Australia": "tntau",
		"Thailand Thai Post": "thailand",
		"SkyNet Malaysia": "skynetmalaysia",
		"马来西亚小包": "malaysiapost",
		"马来西亚大包、EMS": "malaysiaems",
		"京东": "jd",
		"Saudi Post": "saudipost",
		"南非": "southafrican",
		"OCA Argentina": "ocaargen",
		"尼日利亚(Nigerian Postal)": "nigerianpost",
		"Correos Chile": "chile",
		"Israel Post": "israelpost",
		"Toll Priority": "tollpriority",
		"Estafeta": "estafeta",
		"港快速递": "gdkd",
		"Correos de Mexico": "mexico",
		"罗马尼亚": "romanian",
		"TNT Italy": "tntitaly",
		"Mexico Multipack": "multipack",
		"葡萄牙": "portugalctt",
		"Interlink Express": "interlink",
		"DPD UK": "dpduk",
		"华航快递": "hzpl",
		"Gati-KWE": "gatikwe",
		"Red Express": "redexpress",
		"Mexico Senda Express": "mexicodenda",
		"TCI XPS": "tcixps",
		"高铁速递": "hre",
		"新加坡EMS、大包(Singapore Speedpost)": "speedpost",
		"EMS国际": "emsinten",
		"Asendia USA": "asendiausa",
		"Chronopost France": "chronopostfren",
		"Poste Italiane Paccocelere": "italiane",
		"冠达快递": "gda",
		"出口易": "chukou1",
		"黄马甲": "huangmajia",
		"新干线快递": "anlexpress",
		"飞洋快递": "shipgce",
		"贝海国际速递": "xlobo",
		"阿联酋邮政": "emirates",
		"新顺丰": "nsf",
		"巴基斯坦邮政": "pakistan",
		"世运快递": "shiyunkuaidi",
		"合众速递(UCS）": "ucs",
		"阿富汗邮政": "afghan",
		"白俄罗斯": "belpost",
		"全通快运": "quantwl",
		"宅急便": "zhaijibian",
		"EFS Post": "efs",
		"TNT Post": "tntpostcn",
		"英脉物流": "gml",
		"广通速递": "gtongsudi",
		"东瀚物流": "donghanwl",
		"rpx": "rpx",
		"日日顺物流": "rrs",
		"黑猫雅玛多": "yamato",
		"华通快运": "htongexpress",
		"吉尔吉斯斯坦(Kyrgyz Post)": "kyrgyzpost",
		"拉脱维亚(Latvijas Pasts)": "latvia",
		"黎巴嫩(Liban Post)": "libanpost",
		"立陶宛": "lithuania",
		"马尔代夫(Maldives Post)": "maldives",
		"马耳他": "malta",
		"马其顿(Macedonian Post)": "macedonia",
		"新西兰": "newzealand",
		"摩尔多瓦": "moldova",
		"孟加拉国(EMS)": "bangladesh",
		"塞尔维亚(PE Post of Serbia)": "serbia",
		"塞浦路斯": "cypruspost",
		"突尼斯(EMS)": "tunisia",
		"乌兹别克斯坦": "uzbekistan",
		"新喀里多尼亚[法国]": "caledonia",
		"叙利亚": "republic",
		"亚美尼亚": "haypost",
		"也门": "yemen",
		"印度(India Post)": "india",
		"英国(大包,EMS)": "england",
		"约旦": "jordan",
		"越南小包(Vietnam Posts)": "vietnam",
		"黑山(Pošta Crne Gore)": "montenegro",
		"哥斯达黎加": "correos",
		"西安喜来快递": "xilaikd",
		"韩润物流": "hanrun",
		"格陵兰[丹麦]": "greenland",
		"菲律宾": "phlpost",
		"厄瓜多尔(Correos del Ecuador)": "ecuador",
		"冰岛(Iceland Post)": "iceland",
		"波兰小包(Poczta Polska)": "emonitoring",
		"阿尔巴尼亚(Posta shqipatre)": "albania",
		"阿鲁巴[荷兰]": "aruba",
		"埃及": "egypt",
		"爱尔兰(An Post)": "ireland",
		"爱沙尼亚(Eesti Post)": "omniva",
		"云豹国际货运": "leopard",
		"中外运空运": "sinoairinex",
		"上海昊宏国际货物": "hyk",
		"城晓国际快递": "ckeex",
		"匈牙利": "hungary",
		"澳门(Macau Post)": "macao",
		"台湾": "postserv",
		"北京EMS": "bjemstckj",
		"阿根廷(Correo Argentino)": "correoargentino",
		"快淘快递": "kuaitao",
		"秘鲁(SERPOST)": "peru",
		"印度尼西亚EMS(Pos Indonesia-EMS)": "indonesia",
		"哈萨克斯坦(Kazpost)": "kazpost",
		"立白宝凯物流": "lbbk",
		"百千诚物流": "bqcwl",
		"皇家物流": "pfcexpress",
		"法国小包(Colissimo)": "csuivi",
		"奥地利(Austrian Post)": "austria",
		"乌克兰小包、大包(UkrPoshta)": "ukraine",
		"乌干达(Posta Uganda)": "uganda",
		"阿塞拜疆EMS(EMS AzerExpressPost)": "azerbaijan",
		"芬兰(Itella Posti Oy)": "finland",
		"斯洛伐克(Slovenská Posta)": "slovak",
		"埃塞俄比亚(Ethiopian postal)": "ethiopia",
		"卢森堡(Luxembourg Post)": "luxembourg",
		"毛里求斯(Mauritius Post)": "mauritius",
		"文莱(Brunei Postal)": "brunei",
		"Quantium": "quantium",
		"坦桑尼亚": "tanzania",
		"阿曼": "oman",
		"直布罗陀[英国]": "gibraltar",
		"博源恒通": "byht",
		"越南EMS": "vnpost",
		"安迅物流": "anxl",
		"达方物流": "dfpost",
		"伙伴物流": "huoban",
		"天纵物流": "tianzong",
		"波黑": "bohei",
		"玻利维亚": "bolivia",
		"柬埔寨": "cambodia",
		"巴林": "bahrain",
		"纳米比亚": "namibia",
		"卢旺达": "rwanda",
		"莱索托": "lesotho",
		"肯尼亚": "kenya",
		"喀麦隆": "cameroon",
		"伯利兹": "belize",
		"巴拉圭": "paraguay",
		"十方通物流": "sfift",
		"飞鹰物流": "hnfy",
		"i-pacle": "iparcel",
		"鑫锐达": "bjxsrd",
		"百世物流": "baishiwuliu"
	};;
	(function() {
		var show_express_arr = [];
		jSouper.forEach(show_express_names, function(value, key) {
			show_express_arr.push({
				name: key,
				key: value
			});
		});
		jSouper.appReady(function() {
			App.set("$Cache.express", show_express_arr);
		});
		var express_arr = [];
		jSouper.forEach(express_names, function(value, key) {
			express_arr.push({
				name: key,
				value: key,
				search_data: value + key
			});
		});
		jSouper.appReady(function() {
			App.set("$Cache.all_express", express_arr);
		});
	}());
};
_init_express._is_init = false
modulesInit["bus_ach_controller.bus_exporess"] = function(vm) {
	vm.set("$Private.$Event.pays.send_goods", function() {
		var state = vm.get("info.state");
		if (state == appConfig.ACH_STATE["ol未发货"]) {
			var url = appConfig.bus["achs_ol未发货_to_ol未收货"];
		} else if (state == appConfig.ACH_STATE["未发货"]) {
			url = appConfig.bus["achs_未发货_to_已发货"];
		} else {
			alert("error", "错误的订单状态")
			return;
		}
		var express_info = vm.get("info.express_info");
		(window.coAjax || require("coAjax")).put(url + vm.get("_id"), {
			express_info: express_info
		}, function(result) {
			alert("物流设置成功，订单已设置为发货状态");
			vm.set("info.state", result.result);
		});
	});
	vm.set("$Private.$Event.pays.check_express", function() {
		if (vm.get("info.express_info.company")) {
			if (!vm.get("info.express_info.num")) {
				alert("error", "请输入正确的物流编号");
			};
		} else {
			alert("error", "请选择正确的物流公司");
		};
	});
	_init_express();
};
modulesInit["ach_controller.express"] = function(vm) {
	vm.set("$Private.$Event.pays.view_express", function() {
		vm.model.toggle('isexpress');
		(window.coAjax || require("coAjax")).get(appConfig.server_url + "express_search", {
			num: vm.get("info.express_info.num"),
			company: vm.get("info.express_info.company")
		}, function(result) {
			console.log(result.result);
			result.result.data.length = 10;
			vm.set("$Private.$Cache.express_data", result.result);
		})
	});
	vm.set('$Private.$Event.close.express', function() {
		vm.model.toggle('isexpress');
	});
};
modulesInit["save-view-pwd"] = function(vm) {
	var node = vm.getOneElementByTagName("save-view-pwd");;
	jSouper.onElementPropertyChange(node, "value", function(value, key) {
		vm.set("$Private.$Cache.show_value", Array(value.length).join("*"));
	}, true);
	vm.set("$Private.$Event.show_value", function() {

	})
};