var customTagsInit = jSouper.customTagsInit;
customTagsInit["scroll"] = function(vm) {
	var innerHTMLChangeEventFirer = vm.queryElement({
		tagName: "IFRAME"
	})[0];
	innerHTMLChangeEventFirer.onload = function() {
		innerHTMLChangeEventFirer.contentWindow.onresize = _resizeScrollFramework;
	};
	var scrollWrapNode = vm.queryElement({
		tagName: "SCROLLWRAP"
	})[0];
	var scrollNode = vm.queryElement({
		tagName: "SCROLL"
	})[0];
	var scrollBarNode = vm.queryElement({
		tagName: "SCROLLBAR"
	})[0];
	var scrollUpNode = vm.queryElement({
		tagName: "SCROLLUP"
	})[0];
	var scrollBlockNode = vm.queryElement({
		tagName: "SCROLLBLOCK"
	})[0];
	var scrollDownNode = vm.queryElement({
		tagName: "SCROLLDOWN"
	})[0];
	/*
	 * 初始化参数
	 */
	var _animate_time = 0.35;
	var _cumulative = 0;
	var _scrollSpeed = 100;
	var _scrollStyle = scrollWrapNode.style
	var transformAble = _scrollStyle.hasOwnProperty("webkitTransform") || _scrollStyle.hasOwnProperty("mosTransition") || _scrollStyle.hasOwnProperty("oTransition") || _scrollStyle.hasOwnProperty("transition");
	var $scrollWrapNode = $(scrollWrapNode);
	var $scrollNode = $(scrollNode);
	var $scrollBarNode = $(scrollBarNode);
	var $scrollUpNode = $(scrollUpNode);
	var $scrollBlockNode = $(scrollBlockNode);
	var $scrollDownNode = $(scrollDownNode);

	var _upHeight;
	var _downHeight;

	var _windowsHeight;
	var _defauleHeight;
	var _scrollHeight;
	var _blockHeight; //滚动块的高度
	function _initScrollFramework() {

		require(["jQuery", "jQuery.mousewheel"], function($) {

			_upHeight = $scrollUpNode.height();
			_downHeight = $scrollDownNode.height();

			_windowsHeight = $scrollWrapNode.parent().height();
			_defauleHeight = $scrollWrapNode.height();
			_scrollHeight = _defauleHeight - _windowsHeight + 10;

			$scrollWrapNode.height(_windowsHeight);
			if (_windowsHeight / _defauleHeight >= 1) {
				$scrollBarNode.css("display", "none");
				_blockHeight = 0;
			} else {
				$scrollBarNode.css("display", "");
				$scrollBarNode.height(_windowsHeight);
				_blockHeight = (_windowsHeight - _upHeight - _downHeight) * (_windowsHeight / _defauleHeight);
				$scrollBlockNode.height(_blockHeight);
				$scrollDownNode.css("top", _windowsHeight - _upHeight - _blockHeight - _downHeight);
			}

			$scrollBlockNode.addClass("animate");
			if (transformAble) {
				var _scrollAnimte = function(_px) {
					var _block_px = _blockHeight * (-_px / _windowsHeight)
					$scrollNode.css({
						transform: "translateY(" + _px + "px)"
					});
					$scrollBlockNode.css({
						transform: "translateY(" + _block_px + "px)"
					});
				}
			} else {
				var _scrollAnimte = function(_px) {
					var _block_px = _blockHeight * (-_px / _windowsHeight)
					$scrollNode.stop().animate({
						marginTop: _px
					}, _animate_time);
					$scrollBlockNode.stop().animate({
						top: _block_px
					}, _animate_time);
				}
			}

			function scrollEvent(event) {
				event || (event = _vmEvent);
				event.preventDefault();
				var _old_px = _scrollSpeed * _cumulative;
				_cumulative += event.deltaY;
				var _px = _scrollSpeed * _cumulative;
				if (_scrollHeight < -_px) {
					_px = -_scrollHeight;
					_cumulative = _px / _scrollSpeed;
				} else if (_px > 0) {
					_px = 0;
					_cumulative = 0;
				}
				//边界停止
				if (_old_px === _px) {
					//边界动画
					if (_is_by_scroll) { //如果使用滚动条滚动，则取消越界动画
						return
					}
					if (_px === 0) {
						_scrollAnimte(100);
						setTimeout(function() {
							_scrollAnimte(0);
						}, _animate_time * 1000);
					} else if (_px === -_scrollHeight) {
						_scrollAnimte(-_scrollHeight - 100);
						setTimeout(function() {
							_scrollAnimte(-_scrollHeight);
						}, _animate_time * 1000);
					}
					return;
				}
				_scrollAnimte(_px /*, _blockHeight*(-_px/_windowsHeight)*/ );
			}
			//绑定事件
			$scrollWrapNode.on("mousewheel", scrollEvent);

			var _vmEvent = {
				preventDefault: function() {},
				deltaY: 0
			};
			$scrollUpNode.on("click", function(e) {
				_vmEvent.deltaY = 1;
				scrollEvent(_vmEvent);
			});
			$scrollDownNode.on("click", function(e) {
				_vmEvent.deltaY = -1;
				scrollEvent(_vmEvent);
			});

			var $window = $(window);
			var _cacheY;
			var _move_ti;

			function _moveWheel(event) {
				event.preventDefault();
				var _currentY = event.clientY;
				var _disH = _cacheY - _currentY;
				if (!_disH) {
					return
				}
				_cacheY = _currentY;
				_vmEvent.deltaY = (_disH / _blockHeight) * _windowsHeight / _scrollSpeed;
				scrollEvent(_vmEvent);
			}
			var _is_by_scroll = false; //是否滚动条事件在滚动，如果是需要取消滚动越界动画
			//滚动条点击移动事件
			$scrollBlockNode.on("mousedown", function(event) {
				_cacheY = event.clientY;
				_is_by_scroll = true;
				//中断css3平缓
				$scrollBlockNode.removeClass("animate");
				$scrollNode.css("transition", "none");
				$window.on("mousemove", _moveWheel).on("mouseup", function _upmouse() {
					$window.off("mousemove", _moveWheel).off("mouseup", _upmouse);
					//继续CSS3平缓动画
					$scrollBlockNode.addClass("animate");
					$scrollNode.css("transition", "");
					_is_by_scroll = false;
				});
			});
			//over!!
		});
	};
	_initScrollFramework();

	function _resizeScrollFramework() {
		require(["jQuery", "jQuery.mousewheel"], function($) {
			$scrollWrapNode.height("auto");
			_windowsHeight = $scrollWrapNode.parent().height();
			_defauleHeight = $scrollWrapNode.height();
			_scrollHeight = _defauleHeight - _windowsHeight + 10;

			$scrollWrapNode.height(_windowsHeight);
			if (_windowsHeight / _defauleHeight >= 1) {
				$scrollBarNode.css("display", "none");
				_blockHeight = 0;
			} else {
				$scrollBarNode.css("display", "");
				$scrollBarNode.height(_windowsHeight);
				_blockHeight = (_windowsHeight - _upHeight - _downHeight) * (_windowsHeight / _defauleHeight);
				$scrollBlockNode.height(_blockHeight);
				$scrollDownNode.css("top", _windowsHeight - _upHeight - _blockHeight - _downHeight);
			}
		});
	}
};
customTagsInit["inputnumber"] = function(vm) {
	var inputNode = vm.queryElement({
		tagName: "INPUT"
	})[0];
	var numberAddNode = vm.queryElement({
		tagName: "NUMBERADD"
	})[0];
	var numberCutNode = vm.queryElement({
		tagName: "NUMBERCUT"
	})[0];

	function _get_value(num) {
		return parseFloat(num) || 0;
	};
	//动态获取最大最小值，省去绑定
	function _get_max() {
		var max = inputNode.getAttribute("max");
		if (max && jSouper.trim(max)) {
			max = _get_value(max);
		} else {
			max = Infinity;
		}
		return max;
	}

	function _get_min() {
		var min = inputNode.getAttribute("min");
		if (min && jSouper.trim(min)) {
			min = _get_value(min);
		} else {
			min = -Infinity;
		}
		return min;
	}

	function _get_loop() {
		var loop = inputNode.getAttribute("loop");
		if (loop && jSouper.trim(loop)) {
			loop = _get_value(loop) || 1;
		} else {
			loop = 1;
		}
		return loop;
	}

	function _set_input_value(value) {
		// body...
	}
	var old_value = "";

	function _format_value(_is_silence) {
		var value = inputNode.value;
		var number_value = _get_value(value);
		var max = _get_max();
		var min = _get_min();
		if (number_value > max) {
			number_value = max
			numberAddNode.className = "disable"
			numberCutNode.className = ""
		} else if (number_value < min) {
			number_value = min
			numberCutNode.className = "disable"
			numberAddNode.className = ""
		} else if (max == min) {
			numberCutNode.className = "disable"
			numberAddNode.className = "disable"
		} else {
			numberAddNode.className = ""
			numberCutNode.className = ""
		}
		var fixed = +inputNode.getAttribute("fixed");
		isNaN(fixed) && (fixed = 2);
		inputNode.value = number_value.toFixed(fixed);
		if (old_value != inputNode.value) {
			old_value = inputNode.value;
			var _value_map = inputNode.getAttribute("value-map-" + old_value)
			vm.set("$CPrivate.value_map", _value_map);
			if (!_value_map) {
				var _value_map_common_handle = inputNode.getAttribute("value-map-*");
				if ((_value_map_common_handle = vm.get(_value_map_common_handle) || jSouper.App.get(_value_map_common_handle)) instanceof Function) {
					vm.set("$CPrivate.value_map", _value_map_common_handle(old_value));
				}
				/* else if (_value_map_common_handle.indexOf("@moment-format") == 0) {//格式化指令，时间格式化
									require(["moment"])
									vm.set("$CPrivate.value_map", _value_map_common_handle(old_value));
								}*/
			}
			_is_silence || jSouper.dispatchEvent(inputNode, "change");
		}
	};

	function _moni_input() {
		if (inputNode.dispatchEvent) {
			jSouper.dispatchEvent(inputNode, "input");
		} else {
			jSouper.dispatchEvent(inputNode, "keyup");
		}
	}
	vm.set("$CPrivate.Event.formatNumber", _format_value);
	vm.set("$CPrivate.Event.cutNumber", function(e) {
		var loop = _get_loop();
		var fixed = (+inputNode.getAttribute("fixed")) || 2;
		inputNode.value = _get_value(inputNode.value) - loop;
		//先格式化完成数据再触发，keyup是为了触发不支持input event的浏览器
		_format_value();
		// _moni_input();
	});
	vm.set("$CPrivate.Event.addNumber", function(e) {
		var loop = _get_loop();
		var fixed = (+inputNode.getAttribute("fixed")) || 2;
		inputNode.value = _get_value(inputNode.value) + loop;
		_format_value();
		// _moni_input();
	});
	vm.set("$CPrivate.Event.changeNumberByKey", function(e) {
		if (e.which == 38) {
			vm.get("$CPrivate.Event.addNumber").apply(this, arguments);
		} else if (e.which == 40) {
			vm.get("$CPrivate.Event.cutNumber").apply(this, arguments);
		}
	});
	//初始的时候对数值进行一次格式化
	jSouper.onElementPropertyChange(inputNode, "value", function() {
		//只是格式化数据，不触发事件
		_format_value(true);
	}, true);
};
customTagsInit["ueditor"] = function(vm) {
	var ueditorNode = vm.getOneElementByTagName("ueditor");
	var ueNode = vm.getOneElementByTagName("uecontanier");
	var id = ueNode.id = Math.random().toString(36).substr(2);
	var ue;
	var _ue_init_ti;
	var _ue_value = "";
	var _ue_input_key = "";
	require(["ueditor"], function _init_ue() {
		if (document.getElementById(id)) {
			window["ue_" + id] = ue = UE.getEditor(id);

			ue.setOpt("imageCompressEnable", true); //启用压缩
			ue.setOpt("imageCompressBorder", 1200); //多图上传压缩最大宽度

			ue.addListener("contentChange", function _handle_change() {
				if (_ue_input_key) {
					var value = ue.getContent();
					var bind_status_key = ueditorNode.getAttribute("status");
					//判断上传的图片是否还在下载中
					if (value.indexOf('/js/lib/ueditor/themes/default/images/loading.gif"') !== -1) {
						bind_status_key && App.set(bind_status_key, true);
						setTimeout(_handle_change, 200);
					} else {
						bind_status_key && App.set(bind_status_key, false);
					}
					vm.set(_ue_input_key, value);
				}
			});
			ue.on("ready", function() {
				//DOM remove 后，又append，这是UE的isReady任然认为是1，其实iframe需要重载，所以这边又再次出发，value会被正确绑定
				_ue_value && ue.setContent(_ue_value);
			});

			clearTimeout(_ue_init_ti);
			return;
		}
		var args = Array.prototype.slice.call(arguments);
		var self = this;
		_ue_init_ti = setTimeout(function() {
			_init_ue.apply(self, args);
		}, 200)
	});
	//第三方库不稳定，要用try-catch包裹，避免影响到核心业务逻辑
	jSouper.onElementPropertyChange(ueditorNode, "value", function _init_value(key, value) {
		try {
			if (typeof value === "string") {
				_ue_value = value;
			}
			if (ue && value != ue.getContent()) {
				ue.setContent(value);
			}
		} catch (e) {
			console.error("UE init value error");
			console.error(e);
		}
	}, true);
	jSouper.onElementPropertyChange(ueditorNode, "input", function(key, bind_input_key) {
		try {
			(_ue_input_key = bind_input_key) && ue && vm.set(bind_input_key, ue.getContent());
		} catch (e) {
			console.error("UE init inputKey error");
			console.error(e);
		}
	}, true);
};
customTagsInit["paperButton"] = function(vm) {
	var _show_ti;
	vm.set("$CPrivate.$Event.show", function(e) {
		vm.set("$CPrivate.$Cache.show", false);
		clearTimeout(_show_ti);

		vm.set("$CPrivate.$Cache.show", true);
		// console.log(e.pageY,e.offsetY);
		vm.set("$CPrivate.$Cache.top", e.offsetY);
		vm.set("$CPrivate.$Cache.left", e.offsetX);
		_show_ti = setTimeout(function() {
			vm.set("$CPrivate.$Cache.show", false);
		}, 500)
	});
};
// customTagsInit["L:input"] = function() {};
customTagsInit["qrcode"] = function(vm) {
	// var qrcodeNode = vm.queryElement({
	// 	className: "qrcode"
	// })[0];
	var buttonNode = vm.queryElement({
		tagName: "BUTTON"
	})[0];
	require(["common", "coAjax", "copy"], function(jSouper, coAjax, copy) {
		var copyButton = new copy(buttonNode);
		copyButton.on("aftercopy", function(event) {
			alert("链接已经复制到剪切板");
		});

		// function _get_qrcode() {
		// 	var srt = qrcodeNode.getAttribute("srt") || "";
		// 	var logo = qrcodeNode.getAttribute("logo") || "";
		// 	if (!srt) {
		// 		return;
		// 	}
		// 	coAjax.get(appConfig.other.get_qrcode, {
		// 		srt: srt,
		// 		logo: logo
		// 	}, function(result) {
		// 		vm.set("$CPrivate.$Cache.qrcode_url", appConfig.img_server_url + result.result.result.key);
		// 	});
		// }
		// _get_qrcode();
		// jSouper.onElementPropertyChange(qrcodeNode, "srt", _get_qrcode, true);
		// jSouper.onElementPropertyChange(qrcodeNode, "logo", _get_qrcode, true);
	});
	vm.set("$CPrivate.$Event.selectRecUrl", function() {
		this.select && this.select();
	});
};
customTagsInit["copybutton"] = function(vm) {
	var buttonNode = vm.queryElement({
		tagName: "BUTTON"
	})[0];
	require(["common", "copy"], function(jSouper, copy) {
		var copyButton = new copy(buttonNode);
		copyButton.on("aftercopy", function(event) {
			// alert("链接已经复制到剪切板");
			jSouper.dispatchEvent(buttonNode, "aftercopy")
		});
		var _show_success = buttonNode.getAttribute("show-success");
		if (_show_success && _show_success !== "false") {
			alert("信息已经复制到剪切板");
		}
	})
};
customTagsInit["selectsearch"] = function(vm) {
	var node = vm.getOneElementByTagName("selectsearchWrap");
	var optionContainer = vm.getOneElementByTagName("optionContainer");
	var bind_input_key;
	var bind_value;
	jSouper.onElementPropertyChange(node, "input", function(key, value) {
		bind_input_key = value;
	}, true);

	function _check_value_show(key, value) {
		var viewText = bind_value = value;
		jSouper.forEach(optionContainer.getElementsByTagName("OPTIONITEM"), function(optionItemNode) {
			if (optionItemNode.getAttribute("value") == value) {
				viewText = optionItemNode.innerHTML;
				return false;
			}
		});
		vm.set("$CPrivate.$Cache.select_result", viewText);
	}
	jSouper.onElementPropertyChange(node, "value", _check_value_show, true);
	var _optionContainer_innerHTML = optionContainer.innerHTML;
	setInterval(function() {
		var _new_innerHTML = optionContainer.innerHTML
		if (_optionContainer_innerHTML !== _new_innerHTML) {
			_optionContainer_innerHTML = _new_innerHTML;
			_check_value_show("key", bind_value);
		}
	}, 200);

	jSouper.onElementPropertyChange(node, "placeholder", function(key, value) {
		vm.set("$CPrivate.$Cache.placeholder", value || '未选择');
	}, true);

	function _setBindData(cvm, value, viewText) {
		if (bind_input_key) {
			//默认绑定数组对应的item-obj对象
			//如果要绑定value一样的字符串值
			//需要声明use-value-as-result
			if (node.getAttribute("use-value-as-result")) {
				vm.set(bind_input_key, value);
			} else {
				bind_value = cvm ? cvm.get() : value;
				vm.set(bind_input_key, bind_value);
			}
		}
		//使用_check_value_show替代触发改变
		// vm.set("$CPrivate.$Cache.select_result", viewText || value);
	};
	vm.set("$CPrivate.$Event.selected", function(e) {
		var optionItemNode = e.target;
		if (optionItemNode.tagName === "OPTIONITEM") {
			var cvm = vm.getElementViewModel(optionItemNode);
			//绑定显示的值
			var value = optionItemNode.getAttribute("value");
			//绑定反向数据
			_setBindData(cvm, value, optionItemNode.innerHTML);
			jSouper.dispatchEvent(node, "change");
		}
		vm.set("$CPrivate.$Cache.lock_show", false);
	});
	vm.set("$CPrivate.$Event.search", function(e) {
		var input_search_data = vm.get("$CPrivate.$Cache.search_data") || "";
		jSouper.forEach(optionContainer.getElementsByTagName("OPTIONITEM"), function(optionItemNode) {
			var search_data = (optionItemNode.getAttribute("search-data") || optionItemNode.getAttribute("value") || "");
			if (search_data.indexOf(input_search_data) !== -1) {
				var cvm = vm.getElementViewModel(optionItemNode);
				optionItemNode.style.display = "";
			} else {
				optionItemNode.style.display = "none";
			}
		});
		var datalist_key = node.getAttribute("datalist-key");
		var datalist_search_result = [];
		if (datalist_key && input_search_data) {
			var datalist = vm.get(datalist_key);
			if (jSouper.$.isA(datalist)) {
				jSouper.forEach(datalist, function(datainfo) {
					var search_data = (datainfo.search_data || "");
					if (search_data.indexOf(input_search_data) !== -1) {
						datalist_search_result.push(datainfo)
					}
				});
			}
		}
		vm.set("$CPrivate.$Cache.datalist_search_result", datalist_search_result);
	});

	function _showSeach(display) {
		// 如果要隐藏，鼠标不能在plan面板上
		if (!display && vm.get("$CPrivate.$Cache.lock_show")) {
			return
		}
		jSouper.dispatchEvent(node, display ? "beforefocus" : "beforeblur");
		// vm.set("$CPrivate.$Cache.buttontHeight", display ? node.clientHeight : "");
		vm.set("$CPrivate.$Cache.show_search", display);
		// vm.set("$CPrivate.$Cache.planHeight", display ? node.clientHeight : "");
		jSouper.dispatchEvent(node, display ? "afterfocus" : "afterblur");
	};
	vm.set("$CPrivate.$Event.focus_button", function(e) {
		_showSeach(!vm.get("$CPrivate.$Cache.show_search"))
	})
	vm.set("$CPrivate.$Event.hidden_button", function(e) {
		//如果要通过 search-input-blur隐藏。鼠标不能在button上
		if (e.type == "blur" && vm.get("$CPrivate.$Cache.lock_blur")) {
			return
		}
		_showSeach(false);
	});
	vm.set("$CPrivate.$Event.lock_show", function() {
		vm.set("$CPrivate.$Cache.lock_show", true)
	});
	vm.set("$CPrivate.$Event.unlock_show", function() {
		vm.set("$CPrivate.$Cache.lock_show", false)
	});

	vm.set("$CPrivate.$Event.lock_blur", function() {
		vm.set("$CPrivate.$Cache.lock_blur", true)
	});
	vm.set("$CPrivate.$Event.unlock_blur", function() {
		vm.set("$CPrivate.$Cache.lock_blur", false)
	});

	// vm.set("$CPrivate.$Event.focus_input", function() {
	// 	vm.set("$CPrivate.$Cache.focus_input", true);
	// });
	// vm.set("$CPrivate.$Event.hidden_input", function() {
	// 	vm.set("$CPrivate.$Cache.focus_input", false);
	// });
};
customTagsInit["android:selectsearch"] = function(vm) {
	var paperbuttonNode = vm.getOneElementByTagName("android:paperbutton");
	paperbuttonNode.style.display = "inline-block";
	customTagsInit["selectsearch"].call(this, vm);

	vm.set("$CPrivate.$Event.lock_paperbutton_height", function() {
		paperbuttonNode.style.height = paperbuttonNode.clientHeight + "px";
	});
	vm.set("$CPrivate.$Event.unlock_paperbutton_height", function() {
		paperbuttonNode.style.height = "auto";
	});
}
customTagsInit["imagecrop"] = function(vm) {
	var imgNode = vm.getOneElementByTagName("img");
	var imagecropWrapNode = vm.getOneElementByTagName("imagecropWrap");
	require(["coAjax", "cropper"], function(coAjax) {
		var $image = $(imgNode);
		var inputKey;
		var result_url;
		var _inited = false;
		imgNode.onload = function() {
			vm.set("$CPrivate.$Cache.image_loading", false);
		}
		jSouper.onElementPropertyChange(imgNode, "src", function(attr, value) {
			if (value) {
				var new_src = "http://" + location.host + "/proxy?url=" + encodeURIComponent(value);
				if (new_src == imgNode.src) {
					return;
				}
				vm.set("$CPrivate.$Cache.image_loading", true);
				imgNode.src = new_src;
				if (_inited) {
					$image.cropper("destroy");
				}
				$image.cropper({
					multiple: true,
					aspectRatio: +imagecropWrapNode.getAttribute("ratio") || 2.5
				});
				_inited = true;
			}
		}, true);
		jSouper.onElementPropertyChange(imagecropWrapNode, "input", function(attr, value) {
			inputKey = value;
			inputKey && result_url && vm.set(inputKey, result_url);
		}, true);
		vm.set("$CPrivate.$Event.confirm", function() {
			vm.set("$CPrivate.$STATE.uploading", true);
			var imgData = $image.cropper("getDataURL");
			//使用BASE64上传
			coAjax.post(appConfig.other.upload_base64_image, {
				img_base64: imgData
			}, function(result) {
				var img_url = appConfig.img_server_url + result.result.key;
				result_url = img_url;
				if (inputKey) {
					vm.set(inputKey, img_url);
				}
				jSouper.dispatchEvent(imagecropWrapNode, "done");
				vm.set("$CPrivate.$STATE.uploading", false);
				jSouper.dispatchEvent(imagecropWrapNode, "cancel");
			});
		});
		vm.set('$CPrivate.$Event.cancel', function() {
			jSouper.dispatchEvent(imagecropWrapNode, "cancel");
		})
	});
};

customTagsInit["main-goods-mobile"] = function(vm) {
	vm.set("$CPrivate.$Event.change_px", function() {
		if (this.clientWidth > 100) {
			this.setAttribute("src", this.getAttribute("src").replace(/100/g, this.clientWidth))
		}
	});
};
customTagsInit["goods"] = function(vm) {
	var sale = vm.getOneElementByTagName("sale");
	require(['jQuery'], function() {
		if ($(sale).text() != "") {
			// console.log("true");
		} else {
			$(sale).html("0");
		}
	})
};
customTagsInit["pic_wall"] = function(vm) {
	require(["jQuery"], function($) {
		var current_pic;
		setInterval(function _a() {
			var pics = $(".pic");
			current_pic || (current_pic = pics.first());
			pics.fadeOut(800);
			current_pic.stop().fadeIn(800);
			current_pic = current_pic.next();
			if (current_pic.length == 0) {
				current_pic = null;
			}
			return _a;
		}(), 3500);
	});
};
// 新框架中用的
customTagsInit["pic-wall-pc"] = function(vm) {
	require(["jQuery"], function($) {
		var current_pic;
		setInterval(function _a() {
			var pics = $(".pic");
			current_pic || (current_pic = pics.first());
			pics.fadeOut(800);
			current_pic.stop().fadeIn(800);
			current_pic = current_pic.next();
			if (current_pic.length == 0) {
				current_pic = null;
			}
			return _a;
		}(), 5000);
	});
};
customTagsInit["img-uploader"] = function(vm) {
	var uploaderNode = vm.getOneElementByTagName("imgUploaderWrap");
	var inputNode = vm.getOneElementByTagName("input");
	var markNode = vm.getOneElementByTagName("imgMark");

	vm.set("$CPrivate.$Cache.text", "初始化中");
	inputNode.disabled = true;

	function _set_url(url) {
		var _bind_input_key = uploaderNode.getAttribute("input-key");
		if (_bind_input_key) {
			vm.set(_bind_input_key, url);
		}
		//显示预览
		_show_preview(url);
	}
	var _ti;

	function _show_preview(url) {
		clearInterval(_ti);
		//是否在控件上显示图片
		var _one_way = uploaderNode.getAttribute("one-way");
		if (_one_way != "true") {
			vm.set("$CPrivate.$Cache.img_url", url);
			if (!uploaderNode.clientWidth) {
				_ti = setInterval(function() {
					_show_preview(url)
				}, 200);
				//中断
				return;
			}
			vm.set("$CPrivate.$Cache.img_width", uploaderNode.clientWidth);
			vm.set("$CPrivate.$Cache.img_height", uploaderNode.clientHeight);
		}
	}

	function _set_status(value) {
		var _upload_status = uploaderNode.getAttribute("status");
		_upload_status && vm.set(_upload_status, value);
		vm.set("$CPrivate.$Cache.uploading", value);
	}
	require(["jQuery", "coAjax", "localResizeIMG"], function($, coAjax) {
		inputNode.removeAttribute("disabled");
		jSouper.onElementPropertyChange(uploaderNode, "text", function(attr, text) {
			vm.set("$CPrivate.$Cache.text", text || "点击选择文件上传");
		}, true);
		jSouper.onElementPropertyChange(uploaderNode, "url", function(attr, value) {
			_show_preview(value)
		}, true);
		//压缩图片的配置与回调
		var localResizeIMG_config = {
			maxWidth: 1024,
			quality: 1,
			before: function() {
				_set_status(true);
			},
			success: function(result) {
				//使用BASE64上传
				coAjax.post(appConfig.other.upload_base64_image, {
					img_base64: result.base64
				}, function(result) {
					_set_status(false);
					var img_url = appConfig.img_server_url + result.result.key;
					//给绑定的值赋值
					_set_url(img_url);
					//运行回调
					var _upload_callback = uploaderNode.getAttribute("upload-callback");
					if (_upload_callback) {
						var _cb = vm.get(_upload_callback);
						(_cb instanceof Function) && _cb(img_url);
					}
				}, function(errorCode, xhr, errorMsg) {
					_set_status(false);
					alert("error", errorMsg);
				}, function() {
					_set_status(false);
					alert("error", "网络异常，请重试！")
				});
			}
		};
		//动态修改配置
		jSouper.onElementPropertyChange(uploaderNode, "max-width", function(attr, value) {
			var _maxWidth = +uploaderNode.getAttribute("max-width");
			isNaN(_maxWidth) && (_maxWidth = 1024);
			localResizeIMG_config.maxWidth = _maxWidth;
		}, true);
		$inputNode = $(inputNode);
		$inputNode.localResizeIMG(localResizeIMG_config);
	});
	vm.set("$CPrivate.$Event.show_mark", function() {
		markNode.className = "show";
	});
	vm.set("$CPrivate.$Event.hide_mark", function() {
		markNode.className = "";
	});
	vm.set("$CPrivate.$Event.toggle_mark", function() {
		markNode.className = markNode.className ? "" : "show"
	});
	vm.set("$CPrivate.$Event.remove", function() {
		_set_url();
	});
};
customTagsInit["m-img-uploader"] = function(vm) {
	var inputNode = vm.getOneElementByTagName("input");
	var uploaderNode = vm.getOneElementByTagName("mImgUploaderWrap");

	//上传状态，true时表示正在上传。
	function _set_status(value) {
		var _upload_status = uploaderNode.getAttribute("status");
		_upload_status && vm.set(_upload_status, value);
		if (!value) { //false，不在上传状态，重置value
			inputNode.value = null;
			inputNode.removeAttribute("disabled")
		} else {
			inputNode.disabled = true;
		}
	};
	//图片上传
	function _upload_imgs(imgs_base64) {
		var _upload_progress_callback = uploaderNode.getAttribute("upload-progress-callback");
		var _cb = _upload_progress_callback && vm.get(_upload_progress_callback);
		(_cb instanceof Function) || (_cb = jSouper.$.noop);
		_cb(0);
		require("coAjax").post(appConfig.other.upload_mul_base64_image, {
			imgs: imgs_base64
		}, function(result) {
			_set_status(false);
			var result = jSouper.map(result.result, function(img_info) {
				var img_url = appConfig.img_server_url + img_info.key;
				return img_url;
			});
			//给绑定的值赋值
			_set_bind_input(result);
			//运行回调
			var _upload_callback = uploaderNode.getAttribute("upload-callback");
			if (_upload_callback) {
				var _cb = vm.get(_upload_callback);
				(_cb instanceof Function) && _cb(result);
			}
		}, function(errorCode, xhr, errorMsg) {
			_set_status(false);
			var _upload_error_callback = uploaderNode.getAttribute("upload-error-callback");
			if (_upload_error_callback) {
				var _cb = vm.get(_upload_error_callback);
				(_cb instanceof Function) && _cb(errorCode, xhr, errorMsg);
			}
			alert("error", errorMsg);
		}, function() {
			_set_status(false);
			var _upload_error_callback = uploaderNode.getAttribute("upload-error-callback");
			if (_upload_error_callback) {
				var _cb = vm.get(_upload_error_callback);
				(_cb instanceof Function) && _cb();
			}
			alert("error", "网络异常，请重试！")
		}).on("uploadProgress", function(event, position, total, percentComplete) {
			_cb(percentComplete);
		});
	};
	//反向绑定数据
	function _set_bind_input(data) {
		var bind_input_key = uploaderNode.getAttribute("input");
		bind_input_key && vm.set(bind_input_key, data);
	};
	require(["lrz"], function() {
		inputNode.removeAttribute("disabled");

		//压缩图片的配置与回调
		var lrz_config = {
			width: 1280,
			quality: 1
		};
		//图片压缩处理并上传
		vm.set("$CPrivate.$Event.new_goods.multiple_upload_detail_img", function(e) {
			var files = this.files;
			var imgs_base64 = [];
			var imgs_num = Math.min(~~uploaderNode.getAttribute("max-num") || files.length, files.length);
			var progress_num = 0;
			files = Array.prototype.slice.call(files, 0, imgs_num);
			//图片处理进度事件
			var _handle_progress_callback = uploaderNode.getAttribute("handle-progress-callback");
			var _cb = _handle_progress_callback && vm.get(_handle_progress_callback);
			(_cb instanceof Function) || (_cb = jSouper.$.noop);
			_cb(0);

			_set_status(true);
			jSouper.forEach(files, function(file, index) {
				//before event
				setTimeout(function() {
					lrz(file, lrz_config, function(result) {
						//使用BASE64上传
						imgs_base64[index] = result.base64;
						progress_num += 1;
						_cb(progress_num / imgs_num);
						//处理完成，上传
						if (progress_num == imgs_num) {
							_upload_imgs(imgs_base64);
						}
					});
				}, index * 100);
			});
		});
		//动态修改配置
		jSouper.onElementPropertyChange(uploaderNode, "max-width", function(attr, value) {
			var _maxWidth = +uploaderNode.getAttribute("max-width");
			isNaN(_maxWidth) && (_maxWidth = 1280);
			lrz_config.width = _maxWidth;
		}, true);
	});
};
customTagsInit["color-picker"] = function(vm) {
	_input = vm.getOneElementByTagName("input");
	if (_isMobile && _input.type == "color") {
		//如果是支持原生color控件的手机浏览器就用原生；
		vm.set("$CPrivate.use_native", true);
	} else {
		var colorNode = vm.getOneElementByTagName("jqColorPicker");

		function _setValue(value) {
			jSouper.forEach(colorNode.attributes, function(attr) {
				if (attr.name === "input" || attr.name.indexOf("input-") === 0) {
					var key = attr.value;
					vm.set(key, value);
				}
			});
		};
		//获取默认颜色值
		var default_color = "#000000";
		jSouper.onElementPropertyChange(colorNode, "value", function(attrKey, colorValue) {
			colorValue && (default_color = colorValue);
		}, true);

		require(["jQuery", "jQuery.colorPicker"], function($) {
			var $colorNode = $(colorNode);
			$colorNode.ColorPicker({
				color: default_color,
				onShow: function(colpkr) {
					$(colpkr).fadeIn(500);
					return false;
				},
				onHide: function(colpkr) {
					$(colpkr).fadeOut(500);
					return false;
				},
				onChange: function(hsb, hex, rgb) {
					$colorNode.css('backgroundColor', '#' + hex);
					_setValue("#" + hex);
				}
			});
		});
	}
};
customTagsInit["share"] = function(vm) {
	var dataNode = vm.getOneElementByTagName("input");
	require(['common'], function() {
		jSouper.onElementPropertyChange(dataNode, "share-init-key", function(key, share_init_key) {
			if (!share_init_key) {
				return
			}
			var share_text = dataNode.getAttribute("share-text");
			var share_detail = dataNode.getAttribute("share-detail");
			var share_img = dataNode.getAttribute("share-img");
			window._bd_share_config = {
				"common": {
					"bdUrl": App.get('$Cache.recommender_url'),
					"bdSnsKey": {},
					"bdText": share_text,
					"bdDesc": share_detail,
					"bdMini": "2",
					"bdMiniList": false,
					"bdPic": share_img,
					"bdStyle": "0",
					"bdSize": "16"
				},
				"share": {}
			};
			with(document) 0[(getElementsByTagName('head')[0] || body).appendChild(createElement('script')).src = 'http://bdimg.share.baidu.com/static/api/js/share.js?v=89860593.js?cdnversion=' + ~(-new Date() / 36e5)];
		}, true)
	})
};
customTagsInit["noticebox"] = function(vm) {
	// 公告栏
	var noticeboxNode = vm.queryElement({
		className: "noticebox"
	})[0];
	require(["jQuery", 'common'], function($, common) {
		var $noticeboxNode = $(noticeboxNode);
		var i = 1;
		setInterval(function() {
			var $NoticeItemNodes = $noticeboxNode.find(".noticeitem");
			var n = $NoticeItemNodes.length;
			i = (i % n);
			var $firstNoticeItemNode = $NoticeItemNodes.first();
			$firstNoticeItemNode.animate({
				marginTop: -$firstNoticeItemNode.height() * i
			});
			i += 1;
		}, 3000);
		var _ti;
		var time = 0;
	})
};
customTagsInit["jqrcode"] = function(vm) {
	var node = vm.getOneElementByTagName("div");
	var img = vm.getOneElementByTagName("img");
	var options = {
		//误差校正水平
		ecLevel: "M",
		minVersion: 10,
		fill: "#000",
		background: "#fff",

		//文本
		text: "",
		//图片大小
		size: 400,
		//点圆润度
		radius: 0.5,
		//留白
		quiet: 1,

		//Image-Box:4
		mode: 0,

		mSize: 0.2,
		mPosX: 0.5,
		mPosY: 0.5,

		image: img
	};
	var keyMap = {
		"ec-level": "ecLevel",
		"m-size": "mSize",
		"m-posx": "mPosX",
		"m-posy": "mPosY",
		"min-version": "minVersion"
	};
	require(["coAjax", "jQuery", "jQuery.qrcode"], function(coAjax, $) {
		var $node = $(node);
		var _input_key; //向外绑定的key
		var _is_build; //是否编译图片
		var _can_download = _isWX; //是否可下载，手机版不支持base64下载，需要上传服务器，微信中有保存图片的功能，默认开启下载
		jSouper.onElementPropertyChange(node, "input", function() {
			_input_key = node.getAttribute("input");
			_set_bind_input();
		}, true);

		function _url_blob_builder(byteArray, contentType) {
			try {
				var blob = new Blob([byteArray], {
					type: contentType
				});
			} catch (e) {
				// TypeError old chrome and FF
				window.BlobBuilder = window.BlobBuilder ||
					window.WebKitBlobBuilder ||
					window.MozBlobBuilder ||
					window.MSBlobBuilder;
				if (e.name == 'TypeError' && window.BlobBuilder) {
					var bb = new BlobBuilder();
					bb.append([byteArray.buffer]);
					var blob = bb.getBlob(contentType);
				} else if (e.name == "InvalidStateError") {
					// InvalidStateError (tested on FF13 WinXP)
					var blob = new Blob([byteArray.buffer], {
						type: contentType
					});
				} else {
					// We're screwed, blob constructor unsupported entirely   
					throw new Error("your lower mobile~ BLOB");
				}
			}
			var url;
			if (window.webkitURL) {
				url = window.webkitURL.createObjectURL(blob);
			} else if (window.URL && window.URL.createObjectURL) {
				url = window.URL.createObjectURL(blob);
			} else {
				throw new Error("your lower mobile~ URL");
			}
			return url;
		}

		var _can_download_uploader;

		function _set_bind_input() {
			if (_input_key && _is_build) {
				var canvas = $node.find("canvas")[0];
				if (canvas) {
					var contentType = "image/png";
					var data = canvas.toDataURL(contentType);
					// try {
					// 	var b64Data = data.replace("data:" + contentType + ";base64,", "");
					// 	var byteCharacters = atob(b64Data);
					// 	var byteNumbers = new Array(byteCharacters.length);
					// 	for (var i = 0; i < byteCharacters.length; i++) {
					// 		byteNumbers[i] = byteCharacters.charCodeAt(i);
					// 	}
					// 	var byteArray = new Uint8Array(byteNumbers);
					// 	var url = _url_blob_builder(byteArray, contentType);
					// 	// alert("info", url);
					// 	vm.set(_input_key, url);
					// } catch (e) {
					// 	alert("您的浏览器版本过低，不支持下载二维码。" + e.message)
					vm.set(_input_key, data);
					// }
					if (_can_download) {
						_can_download_uploader && _can_download_uploader.abort();
						_can_download_uploader = coAjax.post(appConfig.other.upload_mul_base64_image, {
							imgs: [data]
						}, function(result) {
							var result = jSouper.map(result.result, function(img_info) {
								var img_url = appConfig.img_server_url + img_info.key;
								return img_url;
							});
							var url = result[0]
							vm.set(_input_key, url);
						});
					}
				} else {
					alert("error", "您的浏览器版本过低，不支持下载。");
				}
			}
		};

		function _show_qrcode() {
			clearTimeout(_show_qrcode._ti);
			_show_qrcode._ti = setTimeout(function() {
				console.log(options);
				$node.empty().qrcode(options);
				_is_build = true; //编译过图片
				_set_bind_input();
			});
		};
		img.onload = function() {
			options.mode = 4;
			_show_qrcode();
		}
		jSouper.forEach([
			"radius",
			"m-size",
			"m-posx",
			"m-posy"
		], function(attrKey) {
			jSouper.onElementPropertyChange(node, attrKey, function(attrKey, value) {
				value = parseInt(value, 10) * 0.01;
				if (!isNaN(value)) {
					options[keyMap[attrKey] || attrKey] = value
					_show_qrcode()
				}
			}, true);
		});
		jSouper.forEach([
			"ec-level",
			"fill",
			"background",
			"text"
		], function(attrKey) {
			jSouper.onElementPropertyChange(node, attrKey, function(attrKey, value) {
				value = $.trim(value);
				if (value) {
					options[keyMap[attrKey] || attrKey] = value
					_show_qrcode()
				}
			}, true);
		});
		jSouper.forEach([
			"size",
			"quiet",
			"min-version"
		], function(attrKey) {
			jSouper.onElementPropertyChange(node, attrKey, function(attrKey, value) {
				value = parseInt(value, 10);
				if (!isNaN(value)) {
					options[keyMap[attrKey] || attrKey] = value
					_show_qrcode()
				}
			}, true);
		});
		jSouper.onElementPropertyChange(node, "m-image", function(attrKey, value) {
			value = $.trim(value);
			if (value == img.m_src) {
				//如果src不变是不会触发onload，故不盖被mode值
			} else {
				//滞空，onload会将mode改为4
				options.mode = 0;
			}
			if (value) {
				img.m_src = value;
				img.src = "http://" + location.host + "/proxy?url=" + encodeURIComponent(value);
			}
		}, true);
		if (_isMobile) {
			jSouper.onElementPropertyChange(node, "can-download", function(attrKey, value) {
				_can_download = !!value;
			} /*, true//PS:默认为_isWX*/ );
		}
	});
};

customTagsInit["tinycolorpicker"] = function(vm) {
	var _input = vm.getOneElementByTagName("input");
	if (_isMobile && _input.type == "color") {
		//如果是支持原生color控件的手机浏览器就用原生；
		vm.set("$CPrivate.use_native", true);
		return
	}
	var node = vm.getOneElementByTagName("tinyColorPicker");
	require(["jQuery.tinycolorpicker"], function(tinycolorpicker) {
		var $node = $(node);
		$node.tinycolorpicker();
		$node.on("change", function() {
			var inputKey = node.getAttribute("input");
			if (inputKey) {
				vm.set(inputKey, picker.colorHex);
			}
		});
		var picker = $node.data("plugin_tinycolorpicker");
		jSouper.onElementPropertyChange(node, "value", function(attrKey, value) {
			if (value) {
				picker.setColor(value);
			}
		}, "value")
	});
};

customTagsInit["pagecut"] = function(vm) {
	var node = vm.getOneElementByTagName("pagecut");
	//每页数量
	jSouper.onElementPropertyChange(node, "num", function(attrKey, value) {
		value = ~~value;
		vm.set("$CPrivate.$Cache.num", value);
	}, true);
	//当前页号
	jSouper.onElementPropertyChange(node, "page", function(attrKey, value) {
		value = ~~value;
		vm.set("$CPrivate.$Cache.page", value);
		vm.set("$CPrivate.$Cache.thepagenum", value + 1);
	}, true);
	//页号数组
	jSouper.onElementPropertyChange(node, "total-page", function(attrKey, value) {
		var number_list = [];
		number_list.length = ~~value;
		jSouper.forEach(number_list, function(v, i) {
			number_list[i] = i + 1;
		});
		vm.set("$CPrivate.$Cache.number_list", number_list);
	}, true)
	require(["hash_routie"], function(hash_routie) {
		function _change_page(num, page) {
			hash_routie.hash({
				num: num,
				page: page
			});
			jSouper.dispatchEvent(node, "change");
		};
		vm.set("$CPrivate.$Event.pre_page", function() {
			_change_page(vm.get("$CPrivate.$Cache.num"), vm.get("$CPrivate.$Cache.page") - 1);
		});
		vm.set("$CPrivate.$Event.next_page", function() {
			_change_page(vm.get("$CPrivate.$Cache.num"), vm.get("$CPrivate.$Cache.page") + 1);
		});
		vm.set("$CPrivate.$Event.change_page", function(e, cvm) {
			_change_page(vm.get("$CPrivate.$Cache.num"), cvm.get("$Index"));
		});
	});
};

customTagsInit["addressinput"] = function(vm) {
	var selectNodes = vm.getElementsByTagName("select");
	var provinceNode = selectNodes[0];
	var townNode = selectNodes[1];
	var countyNode = selectNodes[2];
	var detailNode = vm.getOneElementByTagName("input");
	var selectNodesMap = {
		province: provinceNode,
		town: townNode,
		county: countyNode
	};
	var inputNode = vm.getOneElementByTagName("addressinput");
	inputNode.data = {};

	require(["coAjax", "jQuery"], function(coAjax, jQuery) {
		//绑定事件，选择省后
		vm.set("$CPrivate.$Event.province", function(e, cvm, ccvm) {
			if (!ccvm) {
				return;
			}
			inputNode.data.province = ccvm.get();
			var id = ccvm.get("region_id");
			select_xz(id, 'town', '请选择市');
			vm.set("$CPrivate.$Cache.town_disabled", id > 0);
			vm.set("$CPrivate.$Cache.county_disabled", false);
			vm.set("$CPrivate.$Cache.county_list", []);
			emit_change();
			return false;
		});
		//选择市后
		vm.set("$CPrivate.$Event.town", function(e, cvm, ccvm) {
			if (!ccvm) {
				return;
			}
			inputNode.data.town = ccvm.get();
			var id = ccvm.get("region_id");
			select_xz(id, 'county', '请选择地区');
			vm.set("$CPrivate.$Cache.county_disabled", id > 0);
			emit_change();
			return false;
		});

		vm.set("$CPrivate.$Event.county", function(e, cvm, ccvm) {
			if (!ccvm) {
				return;
			}
			inputNode.data.county = ccvm.get();
			emit_change();
			return false;
		});

		vm.set("$CPrivate.$Event.detail", function(e, cvm) {
			inputNode.data.detail = vm.get("$CPrivate.$Cache.detail");
			emit_change();
			return false;
		});

		//简单的回调时间注册表
		var select_cb_map = {};
		//地区选中函数
		function select_xz(id, name) { //id>后台数据id， name=>下拉框name值,titel=>下拉列表首选项
			var selectNode = selectNodesMap[name];
			selectNode && (selectNode.selectedIndex = 0);
			coAjax.get(appConfig.other.get_city + id, function(data) {
				vm.set("$CPrivate.$Cache." + name + "_list", data.result);
				var cb = select_cb_map[name];
				cb instanceof Function && cb(selectNode);
			});
		};

		function emit_change() {
			jSouper.dispatchEvent(inputNode, "change");
		};



		function _onPropertyChange(attrName, attrValue) {
			var selectNode = selectNodesMap[attrName];
			if (attrValue && selectNode.value != String(attrValue)) {

				function set_value() {
					selectNode.value = attrValue;
					jSouper.dispatchEvent(selectNode, "change");
				};
				if (vm.get("$CPrivate.$Cache." + attrName + "_list")) { //如果有列表数据，直接触发
					set_value();
				} else {
					//否则注册回调事件
					select_cb_map[attrName] = set_value;
				}
			}
		};

		jSouper.onElementPropertyChange(inputNode, "province", _onPropertyChange, true);

		jSouper.onElementPropertyChange(inputNode, "town", _onPropertyChange, true);

		jSouper.onElementPropertyChange(inputNode, "county", _onPropertyChange, true);

		jSouper.onElementPropertyChange(inputNode, "detail", function(attrName, attrValue) {
			if (attrValue) {
				vm.set("$CPrivate.$Cache.detail", attrValue);
				jSouper.dispatchEvent(detailNode, "change");
			}
		}, true);

		//选中城市初始化
		select_xz(1, "province");
	});
};

customTagsInit["perm-item"] = function(vm) {
	var node = vm.getOneElementByTagName("li");
	var p_value;
	var p_perms;

	function _check_permission() {
		if (p_perms instanceof Array && p_value) {
			var res = p_perms.indexOf(p_value) === -1;
			node.hidden = res;
		}
	};
	jSouper.onElementPropertyChange(node, "permission-name", function(key, value) {
		p_value = value;
		_check_permission();
	}, true);
	jSouper.onElementPropertyChange(node, "namespace", function(key, value) {
		try {
			p_perms = JSON.parse(value);
		} catch (e) {
			p_perms = null;
		}
		_check_permission();
	}, true);
};
customTagsInit["countdown"] = function(vm) {
	var node = vm.getOneElementByTagName("countdown");
	var options = {};
	jSouper.forEach([
		"now",
		"from",
		"to",
		"interval"
	], function(key) {
		jSouper.onElementPropertyChange(node, key, function(k, v) {
			v = parseInt(v) || 0;
			options[key] = v;
		}, true);
	});

	function _countdown(time) {
		time = +time;
		var value_map = [{
			type: "天 ",
			interval: 24 * 60 * 60 * 1000
		}, {
			type: "小时 ",
			interval: 60 * 60 * 1000
		}, {
			type: "分钟 ",
			interval: 60 * 1000
		}, {
			type: "秒 ",
			interval: 1000
		}];
		var res = "";
		jSouper.forEach(value_map, function(item) {
			var value = time / item.interval;
			if (value > 1) {
				var int_value = parseInt(value);
				time -= int_value * item.interval;
				res += int_value + item.type
			}
			if (value - int_value == 0) { //没有剩余的值， 就不用再算了
				return false;
			}
		});
		return res;
	};
	setTimeout(function _looper() {
		var res;
		var _now = options.now || +new Date;
		if (options.from > _now && options.from < Number.MAX_VALUE) { //如果还没开始，使用from
			res = _countdown(options.from - _now)
		} else if (options.to > _now && options.to < Number.MAX_VALUE) { //还没结束，使用to
			res = _countdown(options.to - _now)
		}
		vm.set("$Private.countdown", res);
		setTimeout(_looper, options.interval || 1000);
	}, options.interval || 1000)
};

customTagsInit["one-hand-select"] = function(vm) {
	var selectNode = vm.getOneElementByTagName("select");
	vm.set("$CPrivate.$Event.mousedown", function(e) {
		e.preventDefault();

		var scroll = this.scrollTop;
		// console.log(e.target);
		e.target.selected = !e.target.selected;

		this.scrollTop = scroll;

		this.focus();
		// setTimeout(function() {
		jSouper.dispatchEvent(selectNode, "change");
		// }, 0);
	});
	vm.set("$CPrivate.$Event.mousemove", function(e) {
		e.preventDefault()
	});
};

//地区选中函数
function select_xz(name, key, vm, region_type, cb) { //id>后台数据id， name=>下拉框name值,titel=>下拉列表首选项
	require(["coAjax"], function(coAjax) {
		// 	coAjax.get(appConfig.other.get_city + name, function(data) {
		// 		vm.set(key, data.result);
		// 		cb instanceof Function && cb(data.result);
		// 	});
		coAjax.get(appConfig.other.get_city_by_name + name, {
			region_type: region_type
		}, function(data) {
			vm.set(key, data.result);
			cb instanceof Function && cb(data.result);
		});
	});
};
customTagsInit['address'] = function(vm) {
	var addressNode = vm.getOneElementByTagName("address");
	//初始化数据
	var _province_map = {
		// 23个省
		"省": ['安徽', '福建', '甘肃', '广东', '贵州',
			'海南', '河北', '河南', '黑龙江', '湖北', '湖南',
			'江苏', '江西', '吉林', '辽宁', '青海',
			'山东', '山西', '陕西', '四川', '台湾', '云南', '浙江'
			// ,'南海诸岛'/*PS，隶属于海南*/
		],
		// 5个自治区
		"自治区": ['新疆', '广西', '宁夏', '内蒙古', '西藏'],
		// 4个直辖市
		"直辖市": ['北京', '天津', '上海', '重庆'],
		// 2个特别行政区
		"特別行政区": ['香港', '澳门']
	};
	var _province_array = [];
	jSouper.forEach(_province_map, function(list, province_type) {
		_province_array.push({
			group_name: province_type,
			list: list
		});
	});
	vm.set("$CPrivate.province_list", _province_array);

	//选择省后
	vm.set("$CPrivate.$Event.province", function(e, cvm) {
		//清空子集数据
		vm.set("$CPrivate.city", "");
		vm.get("$CPrivate.$Event.city")();
		//获取数据
		var region_name = vm.get("$CPrivate.province");
		if (region_name) {
			region_name && select_xz(region_name, '$CPrivate.city_list', vm, "1");
		} else {
			vm.set("$CPrivate.city_list", null);
		}
		var province_input_key = addressNode.getAttribute("province-input");
		if (province_input_key) {
			vm.set(province_input_key, region_name);
		}
		jSouper.dispatchEvent(addressNode, "changeprovince");
	});
	//选择市后
	vm.set("$CPrivate.$Event.city", function(e, cvm) {
		//清空子集的数据
		vm.set("$CPrivate.county", "");
		vm.get("$CPrivate.$Event.county")();
		//获取数据
		var region_name = vm.get("$CPrivate.city");
		if (region_name) {
			region_name && select_xz(region_name, "$CPrivate.county_list", vm, "2");
		} else {
			vm.set("$CPrivate.county_list", null);
		}
		var city_input_key = addressNode.getAttribute("city-input");
		if (city_input_key) {
			vm.set(city_input_key, region_name);
		}
		jSouper.dispatchEvent(addressNode, "changecity");
	});
	//选择县、区后
	vm.set("$CPrivate.$Event.county", function(e, cvm) {
		var region_name = vm.get("$CPrivate.county");
		// if (region_name) {
		var county_input_key = addressNode.getAttribute("county-input");
		if (county_input_key) {
			vm.set(county_input_key, region_name);
		}
		// }
		jSouper.dispatchEvent(addressNode, "changecounty");
	});
	//详细地址
	vm.set("$CPrivate.$Event.detail", function(e, cvm) {
		var detail_input_key = addressNode.getAttribute("detail-input");
		if (detail_input_key) {
			vm.set(detail_input_key, vm.get("$CPrivate.detail"));
		}
		jSouper.dispatchEvent(addressNode, "changedetail");
	});
	/*
	 * 反向绑定
	 */
	jSouper.forEach(["province",
		"city",
		"county",
		"detail"
	], function(key) {
		jSouper.onElementPropertyChange(addressNode, key + "-value", function(k, v) {
			var list = vm.get("$CPrivate." + key + "_list");
			//V可能是完整的地区名，因为显示的只是最短地域名称，所以要做一定的匹配
			if (list instanceof Array) {
				jSouper.forEach(list, function(item) {
					if (v.indexOf(item.region_name) !== -1) {
						v = item.region_name;
						return false;
					}
				});
			}
			vm.set("$CPrivate." + key, v);
			// bind_handle[key](v);
		}, true);
	});
	//反向绑定加载数据的处理函数
	var bind_handle = {
		"province": function(region_name) {
			if (region_name) {
				region_name && select_xz(region_name, '$CPrivate.city_list', vm);
			} else {
				vm.set("$CPrivate.city_list", null);
			}
		}
	};
	//是否显示编辑详细地址
	jSouper.onElementPropertyChange(addressNode, "edit-detail", function(k, v) {
		vm.set("$Private.edit_detail", !!v);
	}, true);
};