var http = require('http');
var querystring = require("querystring");
var urlhandle = require("./url");
http.createServer(function(req, res) {
	// console.log(Object.keys(req), req.method, req.url);
	if (req.method === "GET") {
		var url_hash = req.url.substr(1);
		try {
			res.end(urlhandle.get(url_hash));
		} catch (e) {
			res.end("Error " + e)
		}
	} else if (req.method === "POST") {
		var body = querystring.parse(req.url.substr(2));
		var url = body.url;
		try {
			res.end(urlhandle.build(url));
		} catch (e) {
			res.end("Error " + e)
		}
	}
}).listen(3003, function() {
	console.log("短地址服务开启，监听3003端口");
});