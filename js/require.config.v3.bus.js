(function() {

    /*自定义缓存机制*/
    ; //无本地缓存API的浏览器不搞
    if (!_isLS) {
        return;
    };

    //缓存请求，在iframe载入后统一处理请求
    var _before_onload_stack = [];
    var cb_map = {};
    var _LS_cb_map = {};
    var _LS_set_cb_map = {};

    //指令的ID，确保独立
    var _uuid = 0;

    function _cross_msg() {
        window._getRemoteCache_load = function(text_url, succ_cb, err_cb) {
            var cb_key = text_url + _UUID_SPLIT_ + (_uuid++);
            cb_map[cb_key] = {
                succ_cb: succ_cb,
                err_cb: err_cb
            };
            messenger.targets["cross_iframe"].send(_msg_prefix + cb_key);
        };
        var _globalGet_load = function(key, cb) {
            var cb_key = key + _UUID_SPLIT_ + (_uuid++);
            _LS_cb_map[cb_key] = cb;
            messenger.targets["cross_iframe"].send(_LS_GET_prefix + cb_key);
        };
        var _globalSet_load = function(key, value, succ_cb, err_cb) {
            var cb_key_ext = _UUID_SPLIT_ + (_uuid++);
            _LS_set_cb_map[key + cb_key_ext] = {
                succ_cb: succ_cb,
                err_cb: err_cb
            };
            messenger.targets["cross_iframe"].send(_LS_SET_prefix + JSON.stringify({
                key: key,
                value: value
            }) + cb_key_ext);
        };
        var cross_iframe = document.createElement("iframe");
        cross_iframe.id = "cross_iframe"
        cross_iframe.src = lib_url + "/data.bus.v3.html";
        cross_iframe.style.cssText = "position:absolute;width:0;height:0;display:none;visibility:hidden;top:0;left:0;";
        document.body.appendChild(cross_iframe);
        messenger = new Messenger('cross_iframe', 'O2O_lib');
        console.log("cross_iframe.contentWindow:", cross_iframe.contentWindow);
        cross_iframe.onload = function() {
            // console.log("%c cross_iframe contentWindow LOAD！", "color:blue;");
            //将缓存的参数进行传输
            for (var i = 0, len = _before_onload_stack.length; i < len; i += 1) {
                var require_info = _before_onload_stack[i];
                console.log(require_info);
                messenger.targets["cross_iframe"].send(require_info.type + require_info.value + _UUID_SPLIT_ + require_info.uuid);
            }
            //使用正常的通讯方式进行通信
            window._getRemoteCache = _getRemoteCache_load;
            window.globalGet = _globalGet_load;
            window.globalSet = _globalSet_load;
        };
        messenger.addTarget(cross_iframe.contentWindow, "cross_iframe");
        messenger.listen(function(msg) {
            var msg_info = msg.split(_UUID_SPLIT_);
            msg = msg_info[0];
            var _uuid = msg_info[1] ? (_UUID_SPLIT_ + msg_info[1]) : "";

            if (msg.indexOf(_msg_prefix) !== -1) {
                var res = msg.split(_msg_prefix);
                var text_url = res[0];
                var cache_data = res[1];
                console.log("跨域请求数据缓存完成：", text_url);
                if (cache_data) {
                    var succ_cb = cb_map[text_url + _uuid].succ_cb;
                    succ_cb && succ_cb(cache_data);
                } else {
                    var err_cb = cb_map[text_url + _uuid].err_cb;
                    err_cb && err_cb(cache_data);
                }
            } else if (msg.indexOf(_LS_GET_prefix) === 0) {
                var cache_data = JSON.parse(msg.substr(_LS_GET_prefix.length));
                var cb = _LS_cb_map[cache_data.key + _uuid];
                cb && cb(cache_data.value)
            } else if (msg.indexOf(_LS_SET_prefix) === 0) {
                var cache_data = JSON.parse(msg.substr(_LS_SET_prefix.length));
                if (cache_data.msg === "success") {
                    var succ_cb = _LS_set_cb_map[cache_data.key + _uuid].succ_cb;
                    succ_cb && succ_cb(cache_data);
                } else {
                    var err_cb = _LS_set_cb_map[cache_data.key + _uuid].err_cb;
                    err_cb && err_cb(cache_data);
                }
            }
        });
    };
    if (document.body && document.body.readyState == 'loaded') {
        _cross_msg();
    } else {
        // CODE BELOW WORKS
        if (window.addEventListener) {
            window.addEventListener('load', _cross_msg, false);
        } else {
            window.attachEvent('onload', _cross_msg);
        }
    }
    //frame还没载入前，通知系统是无效的，frame无法收到信息
    window._getRemoteCache_unload = function(text_url, succ_cb, err_cb) {
        var uuid = _uuid++;
        //缓存相应参数
        cb_map[text_url + _UUID_SPLIT_ + uuid] = {
            succ_cb: succ_cb,
            err_cb: err_cb
        };
        _before_onload_stack.push({
            type: _msg_prefix,
            value: text_url,
            uuid: uuid
        });
    };
    var _globalGet_unload = function(key, cb) {
        var uuid = _uuid++;
        _LS_cb_map[key + _UUID_SPLIT_ + uuid] = cb;
        _before_onload_stack.push({
            type: _LS_GET_prefix,
            value: key,
            uuid: uuid
        });
    };
    var _globalSet_unload = function(key, value, succ_cb, err_cb) {
        var uuid = _uuid++;
        _LS_set_cb_map[key + _UUID_SPLIT_ + uuid] = {
            succ_cb: succ_cb,
            err_cb: err_cb,
            uuid: uuid
        };
        _before_onload_stack.push({
            type: _LS_SET_prefix,
            value: JSON.stringify({
                key: key,
                value: value
            }),
            uuid: uuid
        });
    };
    window._getRemoteCache = _getRemoteCache_unload;
    //跨域请求缓存key-value
    window.globalGet = _globalGet_unload;
    window.globalSet = _globalSet_unload;
}());