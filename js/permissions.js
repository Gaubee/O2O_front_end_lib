define(["common",
    "../config/zh-CN/lang.js",
    "../config/zh-CN/database.table.js",
    "../config/zh-CN/database.operate.js",
    "../config/zh-CN/feature.js"
], function(jSouper,
    lang,
    tablesInfo,
    operatesInfo,
    featuresInfo
) {
    //加载数据进行初始化
    $.getJSON("./config/permissions.json", function success(result) {

        function toArray(obj, callback) {
            var result = [];
            jSouper.forEach(obj, function(value, key) {
                var items = {
                    //原始数据
                    key: key,
                    value: value
                }
                callback && (items = callback(items));
                result.push(items);
            });
            return result;
        };

        //格式化配置文件
        // var permissionsInfo = toArray(result, function(item) {
        //     item.title = lang.feature[item.key];
        //     item.operates = toArray(item.value, function(item) {
        //         item.title = lang.operate[item.key];
        //         item.bases = toArray(item.value, function(item) {
        //             item.title = lang.datatable[item.key];
        //             item.operates = toArray(item.value, function(item) {
        //                 return item.key; //lang.operate[item.key];
        //             });
        //             return item;
        //         });
        //         console.log(item.value);
        //         item._other_bases = jSouper.filter(tablesInfo, function(tableIndex) {
        //             return item.value[tableIndex.key]
        //         });
        //         console.log(item._other_bases);
        //         return item;
        //     });
        //     return item;
        // });
        App.set("lang", lang);
        App.set("tables", tablesInfo);
        App.set("operates", operatesInfo);
        App.set("featuresInfo", featuresInfo);
        // console.log(permissionsInfo);
        // App.set("permissions", permissionsInfo);
        App.set("permissions", result);
    });
    //事件注册
    App.set("$Event", {
        add_base: function(e, vm) {
            var new_table_key = vm.get("new_table_key");
            var new_table_value = App.get("lang").datatable[new_table_key];
            var bases = vm.get("bases");
            jSouper.forEach(bases, function(baseItem) {

            })
            bases.push({
                key: new_table_key,
                title: new_table_value
            });
            vm.set("bases", bases);
        }
    })
});
