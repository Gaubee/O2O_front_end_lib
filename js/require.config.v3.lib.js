(function() {

    /*自定义缓存机制*/
    ; //无本地缓存API的浏览器不搞
    if (!_isLS) {
        return;
    };

    var messenger = new Messenger('parent', 'O2O_lib');
    messenger.addTarget(window.parent, "parent");
    messenger.listen(function(msg) {
        var msg_info = msg.split(_UUID_SPLIT_);
        msg = msg_info[0];
        var _uuid = msg_info[1] ? (_UUID_SPLIT_ + msg_info[1]) : "";

        if (msg.indexOf(_msg_prefix) === 0) {
            var text_url = msg.substr(_msg_prefix.length);
            console.log("收到指令下载缓存：", text_url);
            require([text_url], function() {
                messenger.send(text_url + _msg_prefix + LS.get(text_url) + _uuid);
            });
        } else if (msg.indexOf(_css_info_prefix) === 0) {
            var css_files = [];
            LS.each(function(name) {
                if (name.slice(-4) === ".css") {
                    css_files.push(name);
                }
            });
            messenger.send("csslist:" + _css_info_prefix + JSON.stringify(css_files) + _uuid);
        } else if (msg.indexOf(_LS_GET_prefix) === 0) {
            var key = msg.substr(_LS_GET_prefix.length);
            var value = LS.get(key);
            messenger.send(_LS_GET_prefix + JSON.stringify({
                key: key,
                value: value
            }) + _uuid);
        } else if (msg.indexOf(_LS_SET_prefix) === 0) {
            try {
                var info = JSON.parse(msg.substr(_LS_SET_prefix.length));
                LS.set(info.key, info.value);
                var success = true;
            } catch (e) {}
            messenger.send(_LS_SET_prefix + JSON.stringify({
                msg: success ? "success" : "error",
                key: info.key
            }) + _uuid);
        }
    });
}());