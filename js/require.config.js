/*如果有require备份，恢复*/ ;
(function() {
    if (window.requireJsStack) {
        window.require = _require;
        window.define = _define;
        for (var i = 0, len = requireJsStack.length, requireJsItem; requireJsItem = requireJsStack[i]; i += 1) {
            window[requireJsItem.type].apply(window, requireJsItem.args);
        }
    }
}());
var _isIE = !window.dispatchEvent;
var _isDev = location.host.indexOf("dev.dotnar.com") === 0;
var _isDebug = location.search.indexOf("dev=true") !== -1;
var isMobile = function(_mobileAgent) {
    var mobileAgent = _mobileAgent || ["nokia", "iphone", "android", "motorola", "^mot-", "softbank", "foma", "docomo", "kddi", "up.browser", "up.link", "htc", "dopod", "blazer", "netfront", "helio", "hosin", "huawei", "novarra", "CoolPad", "webos", "techfaith", "palmsource", "blackberry", "alcatel", "amoi", "ktouch", "nexian", "samsung", "^sam-", "s[cg]h", "^lge", "ericsson", "philips", "sagem", "wellcom", "bunjalloo", "maui", "symbian", "smartphone", "midp", "wap", "phone", "windows ce", "iemobile", "^spice", "^bird", "^zte-", "longcos", "pantech", "gionee", "^sie-", "portalmmm", "jigs browser", "hiptop", "^benq", "haier", "^lct", "operas*mobi", "opera*mini", "mobile", "blackberry", "IEMobile", "Windows Phone", "webos", "incognito", "webmate", "bada", "nokia", "lg", "ucweb", "skyfire", "ucbrowser"];
    var browser = navigator.userAgent.toLowerCase();
    var isMobile = false;
    for (var i = 0; i < mobileAgent.length; i++) {
        if (browser.indexOf(mobileAgent[i]) != -1) {
            isMobile = true;
            break;
        }
    }
    return isMobile;
};
var _isMobile = isMobile();
/*
 * 本地存储功能
 */
(function(window) {
    //准备模拟对象、空函数等
    var LS, noop = function() {},
        document = window.document,
        notSupport = {
            set: noop,
            get: noop,
            remove: noop,
            clear: noop,
            each: noop,
            obj: noop,
            length: 0
        };

    //优先探测userData是否支持，如果支持，则直接使用userData，而不使用localStorage
    //以防止IE浏览器关闭localStorage功能或提高安全级别(*_* 万恶的IE)
    //万恶的IE9(9.0.11）)，使用userData也会出现类似seesion一样的效果，刷新页面后设置的东西就没有了...
    //只好优先探测本地存储，不能用再尝试使用userData
    (function() {
        // 先探测本地存储 2013-06-06 马超
        // 尝试访问本地存储，如果访问被拒绝，则继续尝试用userData，注： "localStorage" in window 却不会返回权限错误
        // 防止IE10早期版本安全设置有问题导致的脚本访问权限错误
        if ("localStorage" in window) {
            try {
                LS = window.localStorage;
                return;
            } catch (e) {
                //如果报错，说明浏览器已经关闭了本地存储或者提高了安全级别
                //则尝试使用userData
            }
        }

        //继续探测userData
        var o = document.getElementsByTagName("head")[0],
            hostKey = window.location.hostname || "localStorage",
            d = new Date(),
            doc, agent;

        //typeof o.addBehavior 在IE6下是object，在IE10下是function，因此这里直接用!判断
        //如果不支持userData则跳出使用原生localStorage，如果原生localStorage报错，则放弃本地存储
        if (!o.addBehavior) {
            try {
                LS = window.localStorage;
            } catch (e) {
                LS = null;
            }
            return;
        }

        try { //尝试创建iframe代理，以解决跨目录存储的问题
            agent = new ActiveXObject('htmlfile');
            agent.open();
            agent.write('<s' + 'cript>document.w=window;</s' + 'cript><iframe src="/favicon.ico"></iframe>');
            agent.close();
            doc = agent.w.frames[0].document;
            //这里通过代理document创建head，可以使存储数据垮文件夹访问
            o = doc.createElement('head');
            doc.appendChild(o);
        } catch (e) {
            //不处理跨路径问题，直接使用当前页面元素处理
            //不能跨路径存储，也能满足多数的本地存储需求
            //2013-03-15 马超
            o = document.getElementsByTagName("head")[0];
        }

        //初始化userData
        try {
            d.setDate(d.getDate() + 36500);
            o.addBehavior("#default#userData");
            o.expires = d.toUTCString();
            o.load(hostKey);
            o.save(hostKey);
        } catch (e) {
            //防止部分外壳浏览器的bug出现导致后续js无法运行
            //如果有错，放弃本地存储
            //2013-04-23 马超 增加
            return;
        }
        //开始处理userData
        //以下代码感谢瑞星的刘瑞明友情支持，做了大量的兼容测试和修改
        //并处理了userData设置的key不能以数字开头的问题
        var root, attrs;
        try {
            root = o.XMLDocument.documentElement;
            attrs = root.attributes;
        } catch (e) {
            //防止部分外壳浏览器的bug出现导致后续js无法运行
            //如果有错，放弃本地存储
            //2013-04-23 马超 增加
            return;
        }
        var prefix = "p__hack_",
            spfix = "m-_-c", //%
            gthfix = "_WOW_", //!
            reg1 = new RegExp("^" + prefix),
            reg2 = new RegExp(spfix, "g"),
            reg3 = new RegExp(gthfix, "g"),
            encode = function(key) { //编码
                return encodeURIComponent(prefix + key).replace(/\%/g, spfix).replace(/\!/g, gthfix);
            },
            decode = function(key) { //解码
                return decodeURIComponent(key.replace(reg3, "!").replace(reg2, "%")).replace(reg1, "");
            };
        //创建模拟对象
        LS = {
            length: attrs.length,
            isVirtualObject: true,
            getItem: function(key) {
                //IE9中 通过o.getAttribute(name);取不到值，所以才用了下面比较复杂的方法。
                var key_encoded = encode(key)
                return (attrs.getNamedItem(key_encoded) || {
                    nodeValue: null
                }).nodeValue || root.getAttribute(key_encoded);

            },
            setItem: function(key, value) {
                //IE9中无法通过 o.setAttribute(name, value); 设置#userData值，而用下面的方法却可以。
                root.setAttribute(encode(key), value);
                o.save(hostKey);
                this.length = attrs.length;
            },
            removeItem: function(key) {
                //IE9中无法通过 o.removeAttribute(name); 删除#userData值，而用下面的方法却可以。
                try {
                    root.removeAttribute(encode(key));
                    o.save(hostKey);
                    this.length = attrs.length;
                } catch (e) { //这里IE9经常报没权限错误,但是不影响数据存储
                }
            },
            clear: function() {
                while (attrs.length) {
                    this.removeItem(attrs[0].nodeName);
                }
                this.length = 0;
            },
            key: function(i) {
                return attrs[i] ? decode(attrs[i].nodeName) : undefined;
            }
        };
        //提供模拟的"localStorage"接口
        if (!("localStorage" in window))
            window.localStorage = LS;
    })();

    //二次包装接口
    window.LS = !LS ? notSupport : {
        set: function(key, value) {
            //fixed iPhone/iPad 'QUOTA_EXCEEDED_ERR' bug
            if (this.get(key) !== undefined)
                this.remove(key);
            LS.setItem(key, value);
            this.length = LS.length;
        },
        //查询不存在的key时，有的浏览器返回null，这里统一返回undefined
        get: function(key) {
            var v = LS.getItem(key);
            return v === null ? undefined : v;
        },
        remove: function(key) {
            LS.removeItem(key);
            this.length = LS.length;
        },
        removeByPrefix: function(prefix) {
            window.LS.each(function(key, value) {
                if (key.indexOf(prefix) === 0) {
                    window.LS.remove(key)
                }
            });
        },
        clear: function() {
            LS.clear();
            this.length = 0;
        },
        //本地存储数据遍历，callback接受两个参数 key 和 value，如果返回false则终止遍历
        each: function(callback) {
            var list = this.obj(),
                fn = callback || function() {},
                key;
            for (key in list)
                if (fn.call(this, key, this.get(key)) === false)
                    break;
        },
        //返回一个对象描述的localStorage副本
        obj: function() {
            var list = {},
                i = 0,
                n, key;
            if (LS.isVirtualObject) {
                list = LS.key(-1);
            } else {
                n = LS.length;
                for (; i < n; i++) {
                    key = LS.key(i);
                    list[key] = this.get(key);
                }
            }
            return list;
        },
        length: LS.length
    };
    //如果有jQuery，则同样扩展到jQuery
    if (window.jQuery) window.jQuery.LS = window.LS;
})(window);
//缓存获取器
(function() {
    // var _isDev = false;
    window._getCache = function(text_url) {
        var _version = LS.get("appConfig.version");
        if (!_version) {
            LS.removeByPrefix("r_text!");
        } else if (window.appConfig) {
            if (appConfig.version != _version) {
                LS.removeByPrefix("r_text!");
            }
        }
        var _cache = _isDev ? "" : LS.get(text_url);
        return _cache;
    };
}());

// Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function() {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());

/*
 * requireJS配置
 */
(function() {
    var dispatchEvent = window.dispatchEvent;
    /*
     * 基础配置
     */
    window.server_url = _isDev ? "http://" + location.hostname + ":3000/" : "http://api.dotnar.com/";
    window.lib_url = _isDev ? "http://" + location.hostname + ":2221/" : "http://lib.dotnar.com/";
    var requireConfig = window.requireConfig = {
        // baseUrl: "./",
        paths: {
            //应用程序核心
            "jSouper_base": _isDebug ? "http://localhost:9000/build/jSouper" : lib_url + "js/lib/jSouper.min",
            "jSouper": lib_url + "js/lib/jsouper.handler",
            "tools": lib_url + "js/lib/tools",
            "appConfig": window.server_url + "config/config.json?_=" + Math.random(),
            //交互层核心
            "jQuery": lib_url + "js/lib/jquery-1.11.1.min",
            "jQuery.widget": lib_url + "js/lib/jquery.widget.min",
            "jQuery.easing": lib_url + "js/lib/jquery.easing.1.3.min",
            "jQuery.mousewheel": lib_url + "js/lib/jquery.mousewheel",
            "jQuery.colorPicker": lib_url + "js/lib/colorpicker/js/colorpicker",

            //弹出框插件
            "jQuery.notify": lib_url + "js/lib/jquery.notify",

            //二维码应用插件
            "jQuery.qrcode": lib_url + "js/lib/jquery.qrcode.min",

            //hash路由组件
            "routie": lib_url + "js/lib/routie.min",
            //Cookies操作库
            "Cookies": lib_url + "js/lib/cookies.min",

            "shim_json": lib_url + "js/lib/json3",
            "shim_html5": lib_url + "js/lib/html5",
            "flashcanvas": lib_url + "js/lib/FlashCanvasPro/flashcanvas",
            "localResizeIMG": lib_url + "js/lib/localResizeIMG/LocalResizeIMG",
            "ueditor.config": lib_url + "js/lib/ueditor/ueditor.config",
            "ueditor.core": lib_url + "js/lib/ueditor/ueditor.all", //.min
            "ueditor.lang.zh-cn": lib_url + "js/lib/ueditor/lang/zh-cn/zh-cn",
            "ueditor.coAjax_upload": lib_url + "js/lib/ueditor.coAjax_upload",
            "ZeroClipboard": lib_url + "js/lib/ueditor/third-party/zeroclipboard/ZeroClipboard",
            "messenger": lib_url + "js/lib/messenger",

            /*
             * RequireJs 插件，这里命名决定使用前缀，如"css" => "css!"
             */
            //requireJs CSS插件
            "l_css": lib_url + "js/lib/require.css",
            "r_css": lib_url + "js/lib/require.css2",
            //requireJs Text插件
            "l_text": lib_url + "js/lib/require.text",
            "r_text": _isIE ? lib_url + "js/lib/require.text" : lib_url + "js/lib/require.text2",
            //requireJs 国际化插件
            "r_i18n": lib_url + "js/lib/require.i18n",

            /*
             * 通用组件包
             * 包括jSouper、jQuery、require.css"
             */
            "common": lib_url + "js/require.common",
            /*
             * 时间插件
             */
            "moment": lib_url + "js/lib/moment.min",
            "moment-locale-zh-cn": lib_url + "js/lib/moment.locale.zh-cn",
            /*
             * Socket 客户端
             */
            "SockJS": lib_url + "js/lib/sock"
        },
        shim: {
            "jSouper": {
                deps: ["jSouper_base"]
            },
            "jQuery.widget": {
                deps: ["jQuery", "jQuery.easing", "jQuery.mousewheel"]
            },
            "jQuery.easing": {
                deps: ["jQuery"]
            },
            "jQuery.mousewheel": {
                deps: ["jQuery"]
            },
            "jQuery.notify": {
                deps: ["jQuery"]
            },
            "jQuery.qrcode": {
                deps: ["jQuery"]
            },
            "jQuery.colorPicker": {
                deps: ["jQuery", "l_css!" + lib_url + "js/lib/colorpicker/css/colorpicker"]
            },
            "localResizeIMG": {
                deps: _isMobile ? ["jQuery", lib_url + "js/lib/localResizeIMG/patch/mobileBUGFix.mini"] : ["jQuery"]
            },
            "metro": {
                deps: ["jQuery", "jQuery.widget", "metro-core"]
            },
            "moment-locale-zh-cn": {
                deps: ["moment"]
            },

            "ueditor.lang.zh-cn": {
                deps: ["ueditor.core"]
            },
            "ueditor.coAjax_upload": {
                deps: ["ueditor.core"]
            },
            "ueditor.core": {
                deps: ["ueditor.config", "ZeroClipboard"]
            }
        }
    };
    define("ueditor", ["ZeroClipboard", "ueditor.core", "ueditor.lang.zh-cn", "ueditor.coAjax_upload"], function(ZeroClipboard) {
        window.ZeroClipboard = ZeroClipboard;
        return UE
    });
    define("copy", ["ZeroClipboard"], function(ZeroClipboard) {
        ZeroClipboard.config({
            swfPath: lib_url + "js/lib/ueditor/third-party/zeroclipboard/ZeroClipboard.swf"
        });
        return ZeroClipboard;
    });

    if (!dispatchEvent) {
        /*
         * 非现代浏览器的话则加入html5 history垫片
         */
        // requireConfig.shim.routie = {deps:[lib_url+"js/lib/history.min.js"]};
        /*
         * 不支持JSON的浏览器加入JSON3包的支持
         */
        requireConfig.shim.SockJS = {
            deps: ["shim_json"]
        };
    }

    /*
     * 完成配置
     */
    define("require.config", requireConfig)

}());
// /*强行加载配置文件*/
// ;
// (function() {
//     function _getHead() {
//         var head = document.getElementsByTagName('head')[0];
//         //If BASE tag is in play, using appendChild is a problem for IE6.
//         //When that browser dies, this can be removed. Details in this jQuery bug:
//         //http://dev.jquery.com/ticket/2709
//         var baseElement = document.getElementsByTagName('base')[0];
//         if (baseElement) {
//             head = baseElement.parentNode;
//         }
//         return head
//     }
//     var config_url = require.toUrl("appConfig");
//     var scriptNode = document.createElement("script");
//     scriptNode.src = config_url;
//     _getHead().appendChild(scriptNode);
// }());
/*自定义缓存机制*/
; //IE浏览器不搞
_isIE || (function() {
    var _load = requirejs.load;
    var _define = window.define;
    requirejs.load = function(context, moduleName, url) {
        var args = arguments;
        if (moduleName === "r_text") {
            return _load.apply(this, args);
        }
        var text_url = "r_text!" + url;
        var _cache = _getCache(text_url);
        if (!_cache) {
            if (moduleName.indexOf("l_csslib_url") !== -1) {
                debugger
            };
            console.log("getScript:", moduleName, url);
            require(["r_text"], function(r_text) {
                if (r_text.useXhr(url)) {
                    require([text_url], function(text_script) {
                        try {
                            Function(text_script)();
                        } catch (e) {
                            console.error(e)
                        }
                        context.completeLoad(moduleName);
                    });
                } else {
                    _load.apply(this, args);
                }
            })
        } else {
            try {
                Function(_cache)();
            } catch (e) {
                console.error(e)
            }
            context.completeLoad(moduleName);
        }
    }

}());
/*
 * jSouper模板模块的定义与解析
 */
;
(function() {

    /*
     * jSouper的模板文件 ==> UI组件
     * 使用html后缀确保编辑器能自动高亮
     */
    var paths = requireConfig.paths;
    var shim = requireConfig.shim;
    paths["UI.Model.script"] = lib_url + "template/js/xmp.model.tag";
    paths["UI.Model.script2"] = lib_url + "template/js/xmp.model";
    paths["UI.Model.script3"] = lib_url + "template/js/xmp.customTag";
    shim["UI.Model.script"] = shim["UI.Model.script2"] = shim["UI.Model.script3"] = {
        deps: ["jSouper"]
    };
    var jSouperTemplates = {
        "UI.Form": [
            "r_text!" + lib_url + "template/form.html",
            "r_text!" + lib_url + "template/form-input.html",
            "r_css!" + lib_url + "css/jSouper-icons.css",
            "r_css!" + lib_url + "css/metro-calendar.css"
        ],
        "UI.Input": [
            "r_css!" + lib_url + "template/css/xmp.default.css",
            "r_css!" + lib_url + "template/css/form.css"
        ],
        "UI.Model": [
            "UI.Model.script",
            "UI.Model.script2",
            "UI.Model.script3",
            "r_text!" + lib_url + "template/xmp.model.html",
            "r_text!" + lib_url + "template/xmp.model.tag.html",
            "r_text!" + lib_url + "template/xmp.customTag.html",
            "r_css!" + lib_url + "css/icon.css",
            "r_css!" + lib_url + "template/css/xmp.customTag.css"
        ],
        "UI.testing": [
            "r_css!" + lib_url + "css/model.css",
            "r_css!" + lib_url + "css/default.css",
            "r_css!" + lib_url + "css/icon.css",
            "r_text!" + lib_url + "template/xmp.testing.html"
        ]
    };
    var tpl_shims;
    for (var templateName in jSouperTemplates) {
        //生成requireJS的依赖关系数组
        tpl_shims = jSouperTemplates[templateName].slice();
        tpl_shims.unshift("jSouper");
        //定义require的UI模块
        define(templateName, tpl_shims, function(jSouper) {
            var args = Array.prototype.slice.call(arguments);
            args.shift();
            //解析所有模板文件
            for (var i = 0, len = args.length; i < len; i += 1) {
                args[i] && jSouper.parse(args[i]);
            };
            //手动触发更新页面
            jSouper.App && jSouper.App.model.touchOff(".");
        });
    }
}());

require.config(requireConfig);