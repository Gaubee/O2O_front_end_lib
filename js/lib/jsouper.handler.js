define("jSouper", ["jSouper_base", "moment", "moment-locale-zh-cn"], function(jSouper, moment) {
	//使用缓存系统进行获取，无需使用内置请求方式
	// jSouper.config.noCache = true;
	//jSouper自定义函数
	jSouper.registerHandle("#Time", function(time, format, forever) {
		time = (+time == time) ? +time : time;
		if (time >= Number.MAX_VALUE) {
			return forever || "永久";
		};
		var date = moment(time);
		if (format) {
			return date.format(format);
		}
		return date.fromNow() + date.format(" LT");
	});
	jSouper.registerHandle("#IsInfinity", function(value, true_return, false_return) {
		value = (+value == value) ? +value : value;
		if (value >= Number.MAX_VALUE) {
			return true_return;
		} else {
			return false_return || value || "";
		}
	});
	jSouper.registerHandle("#Time_MH", function(time) {
		var date = moment("2001-1-1 " + time);
		return date.format("H:mm");
	});
	jSouper.registerHandle("#Fixed", function(value, fixnum) {
		fixnum = ~~fixnum || 2;
		value = parseFloat(value) || 0;
		value = Math.pow(10, fixnum) * value;
		value = Math.round(value);
		value = value / Math.pow(10, fixnum);
		return value.toFixed(fixnum);
	});
	/*使用缓存*/
	var _ajax = jSouper.$.ajax;
	var _parseANode;
	jSouper.$.ajax = function(config) {
		var text_url = "r_text!" + config.url;
		_parseANode || (_parseANode = document.createElement("a"));
		_parseANode.href = config.url;
		_parseANode.search += (_parseANode.search ? "&" : "?") + "__j__=" + Math.random();
		config.url = _parseANode.href;
		var _cache = _getCache(text_url);
		if (_cache) {
			var xhr = {
				responseText: _cache
			};
			//确保正常的异步流程
			setTimeout(function() {
				config.success && config.success(200, xhr);
				config.complete && config.complete(200, xhr);
			});
			return xhr;
		} else {
			var _success = config.success;
			config.success = function(state, xhr) {
				LS.set(text_url, xhr.responseText);
				_success && _success.apply(this, arguments);
			}
			return _ajax.call(this, config)
		}
	};

	jSouper.registerHandle("#MapName", function(key, obj) {
		(typeof obj == "object") || (obj = {});
		return obj[key];
	});

	jSouper.registerHandle("#MapValue", function(value, obj) {
		if (typeof obj == "object") {
			for (var _key in obj) {
				if (obj[_key] == value) {
					return _key
				}
			}
		}
		return "";
	});

	jSouper.registerHandle("#JSON-Stringify", function(obj) {
		try {
			return JSON.stringify.apply(JSON, arguments);
		} catch (e) {
			return "";
		}
	});

	jSouper.registerHandle("#ArrayOwn", function(arr, item) {
		if (arr instanceof Array) {
			arr = jSouper.map(arr, function(arr_item) {
				if (arr_item instanceof jSouper.ViewModel) {
					return arr_item.get();
				}
				return arr_item;
			});
			return jSouper.indexOf(arr, item) !== -1;
		}
	});
	jSouper.registerHandle("#ArrayNoOwn", function(arr, item) {
		if (arr instanceof Array) {
			arr = jSouper.map(arr, function(arr_item) {
				if (arr_item instanceof jSouper.ViewModel) {
					return arr_item.get();
				}
				return arr_item;
			});
			return jSouper.indexOf(arr, item) === -1;
		}
	});
	jSouper.registerHandle("#IndexOf", function(arr, item, true_return, false_return) {
		true_return === undefined && (true_return = true);
		false_return === undefined && (false_return = false);
		if (arr instanceof Array) {
			arr = jSouper.map(arr, function(arr_item) {
				if (arr_item instanceof jSouper.ViewModel) {
					return arr_item.get();
				}
				return arr_item;
			});
			return jSouper.indexOf(arr, item) !== -1 ? true_return : false_return;
		}else{
			return false_return;
		}
	});
	return jSouper;
});