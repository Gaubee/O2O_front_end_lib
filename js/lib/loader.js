function loader_noop() {};
var addSheet = function(css) {
  css += "\n"; //增加末尾的换行符，方便在firebug下的查看。
  var doc = document,
    head = doc.getElementsByTagName("head")[0],
    styles = head.getElementsByTagName("style"),
    style, media;
  if (styles.length == 0) { //如果不存在style元素则创建
    if (doc.createStyleSheet) { //ie
      doc.createStyleSheet();
    } else {
      style = doc.createElement('style'); //w3c
      style.setAttribute("type", "text/css");
      head.insertBefore(style, null)
    }
  }
  console.log("styles.length:" + styles.length);
  style = styles[0];
  media = style.getAttribute("media");
  if (media === null && !/screen/i.test(media)) {
    style.setAttribute("media", "all");
  }
  if (style.styleSheet) { //ie
    style.styleSheet.cssText += css; //添加新的内部样式
  } else if (doc.getBoxObjectFor) {
    style.innerHTML += css; //火狐支持直接innerHTML添加样式表字串
  } else {
    style.appendChild(doc.createTextNode(css))
  }
  return {
    style: style,
    css: css
  };
};
var removeSheet = function(css) {
  css += "\n"; //增加末尾的换行符，方便在firebug下的查看。
  var doc = document,
    head = doc.getElementsByTagName("head")[0],
    styles = head.getElementsByTagName("style"),
    style, media;
  if (styles.length == 0) { //如果不存在style元素则直接结束
    return
  }

  style = styles[0];
  media = style.getAttribute("media");
  if (media === null && !/screen/i.test(media)) {
    style.setAttribute("media", "all");
  }
  if (style.styleSheet) { //ie
    style.styleSheet.cssText = style.styleSheet.cssText.replace(css, "");
  } else {
    style.innerHTML = style.innerHTML.replace(css, "");
  }
  return {
    style: style,
    css: css
  };
}

var _end_add = false;

function _add_loader() {
  if (_end_add) {
    return;
  }
  //加入loaderHTML
  var loader_id = Math.random().toString(36).substring(2);
  var loaderHTML = '<div id="' + loader_id + '" class="loader-wrap">\
      <div class="loader">\
        <img src="__dotnar_lib_base_url__/images/loader.gif" />\
      </div>\
      <div>' + (function() {
    var result = "";
    if (!window.dispatchEvent) {
      result += "<center>";
      result += "<h3>您的浏览器版本过低</h3>";
      result += "<p>请下载现代浏览器</p>";
      result += "<a href='http://chrome.360.cn/'><img src='http://lib.dotnar.com/images/360jisu.browser.jpg' alt='360极速浏览器'>360极速浏览器</a>";
      result += "<a href='http://ie.sogou.com/'><img src='http://lib.dotnar.com/images/sougou.browser.jpg' alt='搜狗高速浏览器'>搜狗高速浏览器</a>";
      result += "</center>";
    }
    return result;
  }()) + '</div>\
    </div>';
  var cacheDiv = document.createElement("div");
  cacheDiv.innerHTML = loaderHTML;
  var loaderDiv = cacheDiv.children[0];
  document.body.insertBefore(loaderDiv, document.body.children[0]);
  var loaderCSS = 'html, body {\
      min-height: 100%;\
    }\
    \
    .loader-wrap {\
      position: absolute;\
      /*position: fixed!important;*/\
      position: fixed!important;\
      min-height: 100%;\
      min-width: 100%;\
      z-index: 9999;\
      margin: 0px;\
      padding: 0px;\
      border: 0px;\
      left: 0;\
      top: 0;\
      background: radial-gradient(#fff, #F0FFFF);\
    }\
    \
    .loader-wrap .loader{\
      position: absolute;\
      position: fixed!important;\
      top: 0px;\
      text-align: center;\
      bottom: 0px;\
      left: 0px;\
      right: 0px;\
      margin: auto;\
      width: 320px;\
      height: 180px;\
      z-index: 10000;\
    }';
  addSheet(loaderCSS);

  window._removeLoader = function() {
    $loaderDiv = $("#" + loader_id);
    $loaderDiv.stop().animate({
      opacity: 0
    }, 400, function() {
      $loaderDiv.remove();
    });
    document.getElementById("jSouperApp").style.display = "block";
    console.log("程序开始运行");
    _end_add = true;
    window.removeLoader = loader_noop;
    window.showLoader = window._showLoader;
  }
};

function While(obj, key, cb) {
  if (obj[key]) {
    cb()
  } else {
    var __ti = setInterval(function() {
      if (obj[key]) {
        cb();
        clearInterval(__ti);
      }
    }, 10);
  }
}
While(document, "body", _add_loader);

window.removeLoader = function() {
  While(window, "_removeLoader", function() {
    _removeLoader();
  });
};
window.showLoader = loader_noop;

window._showLoader = function() {
  $loaderDiv.stop().css("opacity", 1).appendTo(document.body);
  window.removeLoader = window._removeLoader;
  window.showLoader = loader_noop;
};