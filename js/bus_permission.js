define("bus_permission", function () {
	// _pers_name_map["业务管理"] = {
// 	"orders|order": "订单管理",
// 	"shelves": "新品上架",
// 	"goodsmanager|goodsmanage|update_goods|goodsupdate": "商品管理",
// 	"tile|classify": "分类管理",
// 	"space": "桌座管理",
// 	"evaluation|eva": "评价管理",
// 	"express": "运费模板",
// 	"set_goods_call_back_buff": "支付回调"
// };
// _pers_name_map["营销工具"] = {
// 	"lottery": "抽奖活动",
// 	"qrcode": "二 维 码",
// 	"coupon": "优惠码",
// 	"ticket_factory": "优惠券",
// 	"bus_user": "提现管理",
// 	"points|shoppinguide": "推荐返利",
// 	"bina|bina_user": "分销管理",
// 	"sendmail": "信息群发"
// };
// _pers_name_map["商铺管理"] = {
// 	"storeinfo|changepwd|info": "本站信息",
// 	"setmap": "地图标注",
// 	"themepackage|themeimg|themecolor|theme": "网站风格",
// 	"mainset|noticeset": "广告设置",
// };
// _pers_name_map["其他功能"] = {
// 	"visitorinfo|visitor": "访客信息",
// 	"viplist": "会员列表",
// 	"permission": "子 账 号",
// 	"assshop|assshop-vcode": "店铺关联",
// 	"business": "业务开通",
// 	"invite|invite-bus": "推荐建站"
// };

	var _pers_name_map = {};
	_pers_name_map["业务管理"] = [
		{title:"订单管理", hash:"orders", childs:[
			{title:"订单管理", hash:"orders|order"},
			{title:"等待付款", hash:"orders?type=未付款"},
			{title:"等待发货", hash:"orders?type=未发货"},
			{title:"交易统计", hash:"statistical"}
		]},
		{title:"新品上架", hash:"shelves", childs:[
			{title:"新品上架", hash:"shelves"}
		]},
		{title:"商品管理", hash:"goodsmanager", childs:[
			{title:"商品管理", hash:"goodsmanager|goodsmanage|update_goods|goodsupdate"}
		]},
		{title:"分类管理", hash:"classify", childs:[
			{title:"分类管理", hash:"tile|classify"}
		]},
		{title:"桌座管理", hash:"space-seat", childs:[
			{title:"桌位管理", hash:"space-seat"}
		]},
		{title:"评价管理", hash:"evaluation", childs:[
			{title:"买家评价", hash:"evaluation|eva"}
		]},
		{title:"运费模板", hash:"express", childs:[
			{title:"运费模板", hash:"express"}
		]},
		{title:"支付回调", hash:"set_goods_call_back_buff", childs:[
			{title:"商品支付回调地址管理", hash:"set_goods_call_back_buff"}
		]}
	];
	_pers_name_map["营销工具"] = [
		{title:"抽奖活动", hash:"lottery", childs:[
			{title:"抽奖活动", hash:"lottery"}
		]},
		{title:"二 维 码", hash:"qrcode", childs:[
			{title:"本站二维码", hash:"qrcode"}
		]},
		{title:"优惠促销", hash:"coupon", childs:[
			{title:"优惠码", hash:"coupon"}
		]},
		{title:"优惠券", hash:"ticket_factory", childs:[
			{title:"优惠券", hash:"ticket_factory"}
		]},
		{title:"会员卡", hash:"card_factory", childs:[
			{title:"会员卡", hash:"card_factory"}
		]},
		{title:"提现管理", hash:"bus_user", childs:[
			{title:"提现管理", hash:"bus_user"}
		]},
		{title:"推荐返利", hash:"points", childs:[
			{title:"推荐返利", hash:"points|shoppinguide"}
		]},
		{title:"分销管理", hash:"bina", childs:[
			{title:"商品分销", hash:"bina"},
			{title:"分销人员", hash:"bina_user"}
		]},
		{title:"广告设置", hash:"mainset", childs:[
			{title:"轮播设置", hash:"mainset"},
			{title:"公告栏设置", hash:"noticeset"}
		]},
		{title:"信息群发", hash:"sendmail", childs:[
			{title:"信息群发", hash:"sendmail"}
		]}
	];
	_pers_name_map["本站管理"] = [
		{title:"本站信息", hash:"storeinfo", childs:[
			{title:"本站信息", hash:"storeinfo|info"},
			{title:"修改密码", hash:"changepwd"},
			{title:"修改手机", hash:"change_mobile_phone"}
		]},
		{title:"地图标注", hash:"setmap", childs:[
			{title:"地图标注", hash:"setmap"}
		]},
		{title:"网站风格", hash:"themecolor", childs:[
			{title:"风格包", hash:"themepackage"},
			{title:"背景图片", hash:"themeimg"},
			{title:"整体颜色", hash:"themecolor|theme"}
		]}
	];
	_pers_name_map["其它功能"] = [
		{title:"访客信息", hash:"visitorinfo", childs: [
			{title:"访客信息", hash:"visitorinfo|visitor"}
		]},
		{title:"会员列表", hash:"viplist", childs: [
			{title:"会员列表", hash:"viplist"}
		]},
		{title:"子 账 号", hash:"permission", childs: [
			{title:"子账号管理", hash:"permission"}
		]},
		{title:"店铺关联", hash:"assshop", childs: [
			{title:"店铺关联", hash:"assshop"},
			{title:"申请码", hash:"assshop-vcode"}
		]},
		{title:"业务开通", hash:"business", childs: [
			{title:"店铺续费", hash:"business"}
		]},
		{title:"推荐建站", hash:"invite", childs: [
			{title:"邀请开站", hash:"invite"},
			{title:"入驻商家", hash:"invite-bus"}
		]}
	];
	var _per_keys = [];
	traversal_tree(_pers_name_map, function(child) {
		_per_keys.push.apply(_per_keys, hash_to_keys(child.hash))
	});

	//遍历Pers_tree
	function traversal_tree(tree, cb) {
		for (var name in tree) {
			var _pers = tree[name];
			for (var i = 0, item; item = _pers[i]; i += 1) {
				var _childs = item.childs;
				for (var j = 0, child; child = _childs[j]; j += 1) {
					if (cb(child, j, item, i, name) === false) {
						return
					}
				}
			}
		}
	};

	//把HASH转化为KEYS
	function hash_to_keys(hash) {
		var keys = hash.split("|");
		for (var i = 0, len = keys.length, key; i < len; i += 1) {
			key = keys[i];
			var search_index = key.indexOf("?");
			if (search_index !== -1) {
				key = key.substr(0, search_index);
				keys[i] = key
			}
		}
		return keys;
	};

	//把Tree转化为key-map
	var tree_keys_map = {};
	traversal_tree(_pers_name_map, function(child, _i, item, _j, name) {
		var keys = hash_to_keys(child.hash);
		for (var i = 0, key; key = keys[i]; i += 1) {
			(tree_keys_map.hasOwnProperty(key) ? tree_keys_map[key] : (tree_keys_map[key] = [])).push({
				item: item,
				child: child,
				name: name
			});
		}
	});
	//把KEYS转化为Pers_Tree
	function keys_to_tree(keys, _tree_keys_map) {
		var tree = {};
		_tree_keys_map || (_tree_keys_map = tree_keys_map);
		for (var i = 0, key; key = keys[i]; i += 1) {
			if (_tree_keys_map.hasOwnProperty(key)) {
				var _tree_info = _tree_keys_map[key];
				for (var j = 0, _tree_info_item; _tree_info_item = _tree_info[j]; j += 1) {
					var _tree_pers = tree[_tree_info_item.name] || (tree[_tree_info_item.name] = []);
					for (var _i = 0, _tree_item; _tree_item = _tree_pers[_i]; _i += 1) {
						if (_tree_item.title === _tree_info_item.item.title) {
							break;
						}
					}
					_tree_item || (_tree_item = _tree_pers[_i] = {
						title: _tree_info_item.item.title,
						hash: _tree_info_item.item.hash,
						childs: []
					});
					for (var _j = 0, _tree_child; _tree_child = _tree_item.childs[_j]; _j += 1) {
						if (_tree_child.title === _tree_info_item.child.title) {
							break
						}
					}
					_tree_child || _tree_item.childs.push(_tree_info_item.child)
				}
			}
		}
		return tree;
	};

	//将tree转化成可渲染（编辑与绑定）的数据
	function tree_to_renderable(tree_or_keys) {
		if (tree_or_keys instanceof Array) {
			tree_or_keys = keys_to_tree(tree_or_keys)
		}
		var renderable = [];
		for (var name in tree_or_keys) {
			if (tree_or_keys.hasOwnProperty(name)) {
				renderable.push({
					name: name,
					items: tree_or_keys[name]
				});
			}
		}
		return renderable;
	};
	function renderable_to_tree (renderable) {
		var tree = {};
		for(var i = 0,renderable_item;renderable_item = renderable[i];i+=1){
			tree[renderable_item.name] = renderable_item.items;
		}
		return tree;
	};

	window._pers_item_childs_to_keys = function(childs) {
		var keys = []
		jSouper.forEach(childs, function(child) {
			keys.push.apply(keys, hash_to_keys(child.hash));
		});
		return keys
	};
	window._pers_childs_in_keys = function(keys, hash) {
		if (keys && keys.length) {
			var _child_keys = hash_to_keys(hash);
			return !!jSouper.filter(_child_keys, function(key) {
				return jSouper.indexOf(keys, key) !== -1
			}).length
		}
	};

	var uniq = function(arr) {
		var a = [],
			o = {},
			i,
			v,
			len = arr.length;

		if (len < 2) {
			return arr;
		}

		for (i = 0; i < len; i++) {
			v = arr[i];
			if (o[v] !== 1) {
				a.push(v);
				o[v] = 1;
			}
		}

		return a;
	};

	//获取key对应的title内容
	function keys_to_titles(keys, tree) {
		var titles = [];
		jSouper.forEach(keys, function(key) {
			traversal_tree(tree || _pers_name_map, function(child) {
				if (jSouper.indexOf(hash_to_keys(child.hash), key) !== -1) {
					titles.push(child.title)
				}
			});
		});
		return uniq(titles);
	};

	var _keys_sort_arr = [];
	traversal_tree(_pers_name_map, function(child) {
		_keys_sort_arr.push.apply(_keys_sort_arr, hash_to_keys(child.hash));
	});
	_keys_sort_arr = uniq(_keys_sort_arr);
	//对key进行排序。根据tree进行，统一的顺序
	function sort(keys) {
		keys.sort(function(a, b) {
			return _keys_sort_arr.indexOf(a) - _keys_sort_arr.indexOf(b)
		});
		return keys;
	};

	return {
		sort: sort,
		uniq: uniq,
		traversal_tree: traversal_tree,
		hash_to_keys: hash_to_keys,
		keys_to_tree: keys_to_tree,
		keys_to_titles: keys_to_titles,
		per_keys: _per_keys,
		pers_name_map: _pers_name_map,
		tree_to_renderable: tree_to_renderable,
		renderable_to_tree: renderable_to_tree,
		default_pers: [
			// 业务管理
			"orders",
			"order",
			"statistical",
			"shelves",
			"goodsmanager",
			"goodsmanage",
			"update_goods",
			"goodsupdate",
			"tile",
			"classify",
			"evaluation",
			"eva",
			"express",
			// 营销工具
			"lottery",
			"qrcode",
			"coupon",
			"ticket_factory",
			"bus_user",
			"points",
			"shoppinguide",
			"bina",
			"bina_user",
			"mainset",
			"noticeset",
			// 本站管理
			"storeinfo",
			"info",
			"changepwd",
			"change_mobile_phone",
			"setmap",
			"themepackage",
			"themeimg",
			"themecolor",
			"theme",
			// 其它功能
			"visitorinfo",
			"visitor",
			"viplist",
			"permission",
			"assshop",
			"assshop-vcode",
			"invite",
			"invite-bus"
		]
	};
});
