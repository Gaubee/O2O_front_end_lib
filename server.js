/*
 * 应用启动
 */
var express = require("express");
var app = express();

/*
 * 配置
 */
var port = 2221;

app.all("/*", express.static(__dirname+"/"))

app.listen(port);
console.log("HTTP服务开启在" + port + "端口上。");